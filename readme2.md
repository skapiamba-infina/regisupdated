# REGIS Dashboard
this is my first time writing one of these so be patient with me


## Main JS Files
* storedata.js - Stores scorm variables, modified to work locally as well. Added a function to store a default value if getSuspendData is null

* progress-data.js - Contains all functionality for saving progress of the REGIS Course

    moduleProgress is a map where section-ids point to an array storing the progress of each module in a section.
    note: the array is only filled in as modules are clicked, if nothing is clicked on the transactions module then the whole array will be empty

    true means module is completed, false is incomplete but viewed (half coin), undefined is untouched
    "section-transaction" -> {true,false,undefined,undefined,undefined,false}
    means that Purchase Card is complete, Travel and Special Payment modules are incomplete

* launchpage.js - contains page loading functionality.

    slide-0.html is the dashboard main page, slide-4.html is the quiz slide, and video-slide.html is the video slide. Each slide is loaded with currentPage data that tells it was to put on the slide instead of having a different html file for each page

    only slides that are prerequisites for other slides have ids, as that's the only time you would need access to a specific id for a slide, other slides will have undefined moduleId

```javascript
    //sample page values
    var currentPage =
        {
            sectionId: "section-transations",
            sectionName: "Transactions",
            moduleId: undefined,
            moduleName: "Overtime",
            videoName: "trans_overtime",
            index: 2
        };
```

* dashboardcontrol.js - controls the dashboard, which modules are and aren't active, what they link to when they are clicked, hover tooltips

    Locks all modules and sections to start. then goes through each section and module and checks if stored moduleProgress has any data on them saved

* quiz-data.js - store for a variable that contains all the quiz information

* kc-slider-radio-ckb.js - creates quiz based on quiz data and controls quiz handling

* popups.js - store for a variable that contains all information of popups that show up during videos

* video-slide.js - checks if videos are already completed and can be skipped through, shows any popups that exist for the video as they appear
    All you need to do to add a new slide is make a new link in slide-0.html and then make a video and subtites with that data_video name



The main structure of this project is that there is a main dashboard that has links to videos, and 3 of the links are to quizzes

It's broken down into sections

Currently they are
 # Intro
 # Overviews
 # REGIS Budget Essentials
 # Transactions
 # Administration (newly added)
 # Reports
 # Exporting Transactions & Reports
 # Allocations

User has to complete the Intro section to unlock Overviews, and completion of Overviews marks the course as complete

The modules inside of all sections except for Overviews can be completed in any order, and the course keeps track of 
which modules are 'locked', 'unlocked', 'started but not completed', and 'completed'