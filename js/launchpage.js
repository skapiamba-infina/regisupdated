var currentPage;
var overviewPage;
var startTimeStamp;
var processedUnload;
var reachedEnd = false;

// Load the page with the passed values
function loadPage(secId, secName, title, url, i, modId) {
    currentPage =
        {
            sectionId: secId,
            sectionName: secName,
            moduleId: modId,
            moduleName: title,
            videoName: url,
            index: i
        };
    if (modId != undefined && modId.indexOf("-check") >= 0) {
        modId = modId.substring(0, modId.indexOf("-check"));
        modId += "-overview";
        switch (i) {
            case 1:
                title = "REGIS Overview";
                break;
            case 2:
                title = "Transactions Overview";
                break;
            case 3:
                title = "Reports Overview";
                break;
        }
        overviewPage =
            {
                sectionId: secId,
                sectionName: secName,
                moduleId: modId,
                moduleName: title,
                videoName: url,
                index: i - 1
            };
    }
    setCurrentPage(currentPage);
    loadThePage(currentPage);
}

// Load an overview page (Used when quiz is failed and bottom link is clicked)
function loadOverview(videoName, name) {
    //console.log("loadingOverview");
    //console.log(currentPage);
    //setCurrentPage(overviewPage);
    currentPage.moduleName = name + " Overview"
    currentPage.moduleId = currentPage.moduleId.replace("check", "overview");
    currentPage.videoName = videoName;
    currentPage.index = currentPage.index - 1;
    //console.log(currentPage);
    loadThePage(currentPage);
}

// Unused
function loadPageNumber(page, num) {
    setCurrentPage(currentPage);
    var theIframe = document.getElementById("contentFrame");
    theIframe.src = "slides/slide-" + num + ".html";
}

// Load navigating regis slide
function loadNavigationIntro() {
    currentPage =
        {
            sectionId: "section-intro",
            sectionName: "Introduction to the Regional Information System (REGIS) Virtual Learning Environment (VLE)",
            moduleId: "module-navigating-regis",
            moduleName: "Navigating the REGIS VLE",
            videoName: "navigating_regis",
            index: 1
        };
    
    setCurrentPage(currentPage);
    addToInProgress("section-intro", 1);
    loadThePage(currentPage);
}

// Loads passed page
function loadThePage(page) {
    console.log(page);
    updateCoinVisual();
    currentPage = page;
    setCurrentPage(currentPage);
    var type = "";
    if (currentPage.sectionId == "dashboard")
        loadDashboard();
    else if (currentPage.moduleName == "Knowledge Check") {
        $("#slide-bar").css("display","block");
        loadPageNumber(page, 4);
    }
    else {
        var theIframe = document.getElementById("contentFrame");
        theIframe.src = "slides/video-slide.html";
        $("#slide-bar").css("display","block");
        $("#slide-bar").text("Slide: 1 of 1");
    }
}

function setSlide(current, total)
{
    $("#slide-bar").text('Slide: ' + current + ' of ' + total);
}

// Load dashboard page
function loadDashboard() {
    updateCoinVisual();
    currentPage = {
        sectionId: "dashboard",
        sectionName: "dash",
        videoName: "",
        index: 0
    };
    setCurrentPage(currentPage);
    $("#slide-bar").css("display","none");
    var theIframe = document.getElementById("contentFrame");
    theIframe.src = "slides/slide-0.html";
}

// Return page information
function getPageInfo() {
    return currentPage;
}

function doStart() {
    //get the iFrame sized correctly and set up
    SetupIFrame();

    //record the time that the learner started the SCO so that we can report the total time
    startTimeStamp = new Date();

    //initialize communication with the LMS
    ScormProcessInitialize();
    initSuspendData();
    updateData();

    // Cheat function for finishing slides
    $("#coinss").click(function (e) {
        //cheat();
    });

    $(".coin-counter").find("span").text("x " + getCoinCount());

    //close popup window
    $(".close-window").click(function () {
        $(this).parent().parent().parent().css("display", "none");
        $("#dark-cover").css("display", "none");
    });

    //"Navigating Regis" link in help menu
    $("#navigation-link").click(function () {
        $(this).closest(".vifta-modal").css("display", "none");
        $("#dark-cover").css("display", "none");
        loadNavigationIntro();
    });

    var curPage = getCurrentPage();
    if (curPage == undefined) {
        curPage = {
            sectionId: "dashboard",
            sectionName: "dash",
            moduleName: "",
            videoName: "",
            index: 0
        };
    }

    $(".show-window").off("click");
    
    //Shows popup windows
    $(".show-window").click(function () {
        var name = $(this).attr("title");
        if (name == "Toolbox") {
            $("#toolbox-dialog").css("display", "block")
        }
        else if (name == "Help") {
            $("#help-dialog").css("display", "block")
        }
        $("#dark-cover").css("display", "block");
    });

    $("#close-scorm").click(function () {
        doUnload(true);
    });

    loadThePage(curPage);
}

function doUnload(pressedExit) {
    //storeTempObjToSuspendData();
    //don't call this function twice
    if (processedUnload == true) {
        return;
    }

    processedUnload = true;

    //record the session time
    var endTimeStamp = new Date();
    var totalMilliseconds = (endTimeStamp.getTime() - startTimeStamp.getTime());
    var scormTime = ConvertMilliSecondsToSCORMTime(totalMilliseconds, false);

    ScormProcessSetValue("cmi.core.session_time", scormTime);

    //if the user just closes the browser, we will default to saving 
    //their progress data. If the user presses exit, he is prompted.
    //If the user reached the end, the exit normall to submit results.
    //if (pressedExit == false && reachedEnd == false) {
    //ScormProcessSetValue("cmi.core.lesson_status", 'incomplete');
    ScormProcessSetValue("cmi.core.exit", "suspend");

    //}
    //API.LMSCommit("");

    SCORM_CommitData();
    ScormProcessFinish();
}

function arraySearch(arr, val) {
    for (var i = 0; i < arr.length; i++)
        if (arr[i] === val)
            return i;
    return false;
}

function isElms() {
    try {
        var isScoreworking = SCORM_GetScore();
        if (isScoreworking !== null)
            return 1;
        else
            return 0;
    }
    catch (err) {
        return null;
    }
}

function SetupIFrame() {
    //set our iFrame for the content to take up the full screen except for our navigation
    var navWidth = -20;
    setIframeHeight("contentFrame", navWidth);
    //need this in a setTimeout to avoid a timing error in IE6
    window.setTimeout('window.onresize = function() { setIframeHeight("contentFrame", ' + navWidth + '); }', 1000);
}

function setIframeHeight(id, navWidth) {
    if (document.getElementById) {
        var theIframe = document.getElementById(id);
        if (theIframe) {
            var height = getWindowHeight();
            theIframe.style.height = Math.round(height) - navWidth + "px";
            theIframe.style.marginTop = Math.round(((height - navWidth) - parseInt(theIframe.style.height)) / 2) + "px";
        }
    }
}

function getWindowHeight() {
    var height = 0;
    if (window.innerHeight) {
        height = window.innerHeight - 18;
    } else if (document.documentElement && document.documentElement.clientHeight) {
        height = document.documentElement.clientHeight;
    } else if (document.body && document.body.clientHeight) {
        height = document.body.clientHeight;
    }
    return height;
}