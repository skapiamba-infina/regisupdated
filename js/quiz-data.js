var quizName;
var quizId;
var passingScore;
var questions;
var videoName;

function setQuizData(quiz) {

    if (quiz == "module-regis-check") {
        quizName = "REGIS";
        quizId = quiz;
        passingScore = 80;
        videoName = "regis_overview";
        questions = [
            {
                question: "What is the purpose of REGIS?",
                choices: ['a', 'b', 'c', 'd'],
                choicesText: ['To implement internal controls around the use of money', 'To track funds', 'To have a standardized cuff record system', 'All of the above'],
                correctAnswer: 3,
                answered: false,
                rightText: "REGIS is a financial cuff record system that allows users to allocate, track, record, manage, and reconcile financial transactions against DELPHI. REGIS is an example of an internal control around the use of money."
            },
            {
                question: "REGIS has the ability to track all types of funds. However, it is designated as the FAA’s official cuff record for the following funds:",
                choices: ['a', 'b', 'c', 'd'],
                choicesText: ['Operations', 'Facilities & Equipment Activity 5, referred to as Activity 5', 'Airports Administrative Funds', 'All of the above'],
                correctAnswer: 3,
                answered: false,
                rightText: 'In FY14 REGIS was designated as the FAA’s official agency-wide cuff record system for tracking Operations, Facilities & Equipment Activity 5, and Airports Administrative funds. However, depending on the Line of Business or Staff Office, F&E Direct, Reimbursable, and other "colors of money" are also tracked in REGIS. Check with your LOB/SO Senior Financial Manager for information on REGIS implementation in your organization.'
            }, {
                question: "How do you obtain a REGIS password?",
                choices: ['a', 'b', 'c', 'd'],
                choicesText: ['Call the NSC Helpdesk', 'Obtain COR Approval', 'Contact your LOB/SO Financial Manager', 'Contact your REGIS POC'],
                correctAnswer: 3,
                answered: false,
                rightText: 'Your REGIS POC will provide your REGIS username and password. It is recommended that you change your password the first time you log in.'
            }, {
                question: "How can you tell if you do not have access to a REGIS module?",
                choices: ['a', 'b', 'c', 'd'],
                choicesText: ['It does not appear on the Launchpad', 'It is greyed out on the Launchpad', 'Your REGIS POC will tell you', 'It appears in red on the Launchpad'],
                correctAnswer: 1,
                img: 'kc_regis-question7.jpg',
                answered: false,
                rightText: 'The modules that you have access to are enabled. The others will be greyed out. If you believe you should have access to a module that is not enabled, contact your REGIS POC.',
                wrongText: 'REGIS modules that you do not have access to are greyed out. If you believe you should have access to a module that is not enabled, contact your REGIS POC.'
            }, {
                question: "What does the HELP button do?",
                choices: ['a', 'b', 'c', 'd'],
                choicesText: ['Allows the user to send an email to the REGIS Team', 'Displays the REGIS Team POCs and contact information', 'Displays information about the window you are currently viewing', 'None of the above'],
                correctAnswer: 2,
                img: 'kc_regis-question7.jpg',
                answered: false,
                rightText: 'The "Help" button will display pop-up information about the current window you are viewing.'
            }, {
                question: "Match the dates that appear at the bottom of the REGIS Launchpad with their definition:",
                subquestions: ['DELPHI Latest Process Date', 'PA Latest Process Date', 'Overnight Report Latest Process Date'],
                choices: {
                    a: 'Date that is color-coded red or blue depending upon if the process has been completed',
                    b: 'Date in which data is downloaded from the National Data Center, or NDC',
                    c: 'Date in which the Project Authorizations are downloaded from the Funds Control Module, or FCM, into REGIS'
                },
                correctAnswer: ['b', 'c', 'a'],
                img: 'kc_regis-question7.jpg',
                answered: false,
                rightText: 'You now understand what all of the dates on the REGIS Launchpad mean.',
                wrongText: 'You have matched at least one date with the wrong definition. There is a cheat sheet in the Toolbox that talks about these dates.'
            }, {
                question: "You must have Citrix installed on your computer in order to access REGIS.",
                choices: ['a', 'b'],
                choicesText: ['True', 'False'],
                correctAnswer: 0,
                answered: false,
                rightText: 'You must have Citrix installed on your computer and a REGIS username and password to access REGIS.'
            }
        ];
    }
    else if (quiz == "module-transaction-check") {
        quizName = "Transactions";
        quizId = quiz;
        passingScore = 80;
        videoName = "trans_overview";
        questions = [
            {
                question: "The _________ icon allows you to prioritize, save, and delete your most commonly used accounting strings.",
                choices: ['a', 'b', 'c', 'd'],
                choicesText: ['Transactions', 'Save', 'Favorites', 'None of the above'],
                correctAnswer: 2,
                rightText: "Favorites can save you time and effort if you commonly use the same accounting string(s).",
                wrongText: "The Favorites icon allows you to prioritize, save, and delete your most commonly used accounting strings. Favorites can save you time and effort if you commonly use the same accounting string(s)."
            },
            {
                question: "Which PC&B transactions do users have to manually enter into REGIS?",
                choices: ['a', 'b', 'c', 'd'],
                choicesText: ['Holiday pay, cash awards, and permanent change of station', 'Overtime, cash awards, and holiday pay', 'Overtime, cash awards, and permanent change of station', 'All pay-related transactions'],
                correctAnswer: 2,
                rightText: "Overtime, cash awards, and permanent change of station must be manually entered into REGIS. REGIS imports all other PC&B transactions."
            },
            {
                question: "In a REGIS transaction, everything above the yellow line is core data and is consistent in all transactions.",
                choices: ['a', 'b'],
                choicesText: ['True', 'False'],
                correctAnswer: 0,
                rightText: "The information above the yellow line will be the same regardless of the transaction type."
            },
            {
                question: "In a REGIS transaction, everything below the yellow line is transaction specific.",
                choices: ['a', 'b'],
                choicesText: ['True', 'False'],
                correctAnswer: 0,
                rightText: "The data below the yellow line will contain different information depending on the transaction type."
            },
            {
                question: "What is the name of the field that uniquely identifies a transaction?",
                choices: ['a', 'b', 'c', 'd'],
                choicesText: ['Transaction Name', 'Transaction ID', 'Transaction History', 'Transaction Register'],
                correctAnswer: 1,
                rightText: "Every transaction will have a unique Transaction ID.",
                wrongText: "The Transaction ID uniquely identifies a transaction."
            },
            {
                question: "Your view and access to Program Names, Region Codes, and Org Codes are _____ based.",
                choices: ['a', 'b', 'c'],
                choicesText: ['Password', 'Position', 'Organization'],
                correctAnswer: 1,
                rightText: "When your REGIS username was established, your REGIS POC associated positions with your username. These positions give you the rights to certain Program Names, Region Codes, and Org Codes. If you don’t have the required access or if you believe your position is set up incorrectly, contact your REGIS POC.",
                wrongText: "The Program Names, Region Codes, and Org Codes that are available to you are Position based."
            },
            {
                question: "If you have selected a position with one Region and one Org Code associated with it, the filter for Region and Org Code will be pre-populated.",
                choices: ['a', 'b'],
                choicesText: ['True', 'False'],
                correctAnswer: 0,
                rightText: "If you selected a position that only has one Region and Org Code associated with it, the \"Region Code\" and \"Org Code\" fields are pre-filled based on that position. If you selected all positions, or a position associated with multiple Region and Org Codes, you will have to choose your \"Region Code\" and \"Org Code\" from the drop-down lists."
            },
            {
                question: "The fund field in the Transaction Register window defaults to:",
                choices: ['a', 'b', 'c', 'd'],
                choicesText: ['All Active Funds', 'The fund you filtered for', 'The last fund you viewed in the window', 'There is no default, it is blank'],
                correctAnswer: 0,
                rightText: "The fund field will always default to \"All Active Funds\", even if you were using a different fund the last time you were in the system."
            },
            {
                question: "Which is not part of the DELPHI accounting string?",
                choices: ['a', 'b', 'c', 'd'],
                choicesText: ['Region Code and Asset Code', 'Program Name and Program Category', 'Org Code and Activity Code', 'None of the above'],
                correctAnswer: 1,
                rightText: "Neither Program Name nor Category is part of the DELPHI accounting string. When REGIS is set up for an organization, the Senior Financial Manager establishes the Program Names and Program Categories.",
                wrongText: "Program Name and Program Category are not part of the DELPHI accounting string. When REGIS is set up for an organization, the Senior Financial Manager establishes the Program Names and Program Categories."
            },
            {
                question: "The DELPHI Expenditure Type corresponds to what element in REGIS?",
                choices: ['a', 'b', 'c', 'd'],
                choicesText: ['Loc ID', 'Object Class Code', 'Program Category', 'None of the above'],
                correctAnswer: 1,
                rightText: "The DELPHI Expenditure Type is referred to as the Object Class Code in REGIS."
            },
            {
                question: "The DELPHI Expenditure Organization Code is broken into which two pieces in REGIS:",
                choices: ['a', 'b', 'c', 'd'],
                choicesText: ['Region and Activity Code', 'Region and Org Code', 'Asset Code and Activity Code', 'None of the above'],
                correctAnswer: 1,
                rightText: "The first two characters of the Expenditure Organization Code indicate the Region and the last eight characters make up the Cost Center Code, which in REGIS, is called the Org Code.",
                wrongText: "The DELPHI Expenditure Organization Code is broken into the Region Code and the Org Code. The first two characters of the Expenditure Organization Code indicate the Region and the last eight characters make up the Cost Center Code, which in REGIS, is called the Org Code."
            },
            {
                question: "Match the Save options with their definition:",
                subquestions: ["Saves the transaction, generates a Transaction ID, and copies all fields into a new transaction", "Saves the transaction, generates a Transaction ID, and closes the window", "Saves the transaction, generates a Transaction ID, and clears all the fields so you can enter a new transaction", "Saves the transaction, generates a Transaction ID, and leaves the transaction open for further review or modification",],
                choices: {
                    a: 'Save',
                    b: 'Save and Exit',
                    c: 'Save and Copy',
                    d: 'Save and New',
                },
                correctAnswer: ['c', 'b', 'd', 'a'],
                img: 'kc_transactions-question12.jpg',
                rightText: "You now understand all of the Save options.",
                wrongText: "You have matched at least one Save option with the wrong definition. There is a cheat sheet in the Toolbox that talks about these options."
            },
            {
                question: "You can only modify the seven key DELPHI elements if the transaction has not been reconciled.",
                choices: ['a', 'b'],
                choicesText: ['True', 'False'],
                correctAnswer: 0,
                rightText: "You can only modify the seven key DELPHI elements if the transaction has not been reconciled. REGIS auto-generates a warning message when you attempt to open a reconciled record. This helps prevent users from unintentionally altering reconciled records."
            },
            {
                question: "In order to export from REGIS, you must set up a folder on:",
                choices: ['a', 'b', 'c', 'd'],
                choicesText: ['Your Desktop', 'C$ on Client', 'Outlook', 'Any of the above'],
                correctAnswer: 1,
                rightText: "Saving it on any other drive may result in your document being lost or stored somewhere you cannot retrieve it.",
                wrongText: "In order to export from REGIS, you must set up a folder on C$ on Client. Saving it on any other drive may result in your document being lost or stored somewhere you cannot retrieve it."
            },
            {
                question: "A reconciled record is:",
                choices: ['a', 'b', 'c', 'd', 'e'],
                choicesText: ['A record that has been matched to a corresponding record in DELPHI', 'A record that has been matched to a corresponding record in the applicable transactional system (i.e. US Bank, PRISM, E2)', 'A record that has been created in REGIS', 'A record that has been created in DELPHI', 'None of the above'],
                correctAnswer: 0,
                rightText: "A record is only considered reconciled when a REGIS transaction is matched with its corresponding DELPHI record.",
            },
            {
                question: "Deleted records can be restored.",
                choices: ['a', 'b'],
                choicesText: ['True', 'False'],
                correctAnswer: 1,
                rightText: "Once you delete a REGIS record, it cannot be restored."
            },
            {
                question: "The Transaction Register window shows:",
                choices: ['a', 'b', 'c', 'd'],
                choicesText: ['Only the transaction records that you have entered', 'All transaction records that meet the filter criteria selected', 'All transaction records entered by your organization', 'None of the above'],
                correctAnswer: 1,
                rightText: "The Transaction Register window shows all transactions that meet the filter criteria selected, regardless of who entered them. You may see transactions entered by other users."
            },
            {
                question: "What are some ways you can customize your grid view?",
                choices: ['a', 'b', 'c', 'd', 'e'],
                choicesText: ['Drag and drop columns', 'Show/Hide columns', 'Group by columns', 'Sort ascending', 'All of the above'],
                correctAnswer: 4,
                rightText: "All of these are ways to customize the grid view."
            },
            {
                question: "Right clicking in the transaction grid, and selecting the \"New Filter\" option does the following:",
                choices: ['a', 'b', 'c', 'd'],
                choicesText: ['Takes you back to your current filter', 'Adjusts the width of a column to best fit the data in that column', 'Pulls up a new blank filter and deletes any filters you have already applied', 'Allows you to sort the data by up to eight different fields'],
                correctAnswer: 2,
                rightText: "The \"New Filter\" option pulls up a new blank filter. It also deletes any filters you have already applied."
            },
            {
                question: "The funnel icon next to each column’s header allows the user to:",
                choices: ['a', 'b', 'c', 'd'],
                choicesText: ['Delete a record', 'Reconcile a record', 'Add additional filters', 'None of the above'],
                correctAnswer: 2,
                rightText: "The funnel icon allows the user to add additional filters to the filters already applied to the data grid."
            },
        ];

    }
    else if (quiz == "module-reports-check") {
        quizName = "Reports";
        quizId = quiz;
        passingScore = 80;
        videoName = "reports_overview";
        questions = [
            {
                question: "Running reports will directly impact the data within the REGIS Transactions module.",
                choices: ['a', 'b'],
                choicesText: ['True', 'False'],
                correctAnswer: 1,
                rightText: "Running reports will not directly impact data within the REGIS Transactions module. For example, running the Transaction Register Report will not impact data in the Transactions module."
            },
            {
                question: "How can you get a description of a report?",
                choices: ['a', 'b', 'c'],
                choicesText: ['Click on the report name', 'Hover over the report name', 'Ask your REGIS POC'],
                correctAnswer: 1,
                rightText: "Once you are in the Reports module, you can hover over each report to see a brief description of the report. Be sure to expand any collapsed folders to see all of the report names.",
                wrongText: ""
            },
            {
                question: "_________ Reports export REGIS and DELPHI data into an Excel workbook. This allows for customized and ad hoc reporting.",
                choices: ['a', 'b', 'c', 'd'],
                choicesText: ['Summary', 'Pivot', 'Reimbursable', 'Detail'],
                correctAnswer: 1,
                rightText: "Pivot Reports include reports that export REGIS and DELPHI data into an Excel Workbook. This allows for customized and ad hoc reporting. It also allows you to manipulate and analyze the data. Be sure to take the Pivot Report and Customized Report lessons for more information on these reports."
            },
            {
                question: "You can run REGIS reports that contain the following sources of data:",      //Has an image in the answer. CAUSE OF COURSE IT WANTS TO BE HARD
                choices: ['a', 'b', 'c', 'd', 'e', 'f'],
                choicesText: ['REGIS only', 'DELPHI only', 'REGIS and PRISM', 'REGIS and DELPHI', 'A, B, and D', 'All of the above'],
                correctAnswer: 4,
                rightText: "REGIS allows you to run reports that include only REGIS data, only DELPHI data, or both. The various types of reports in the Reports module are categorized by the data they include."
            },
            {
                question: "As a general rule of thumb, any report with the word \"Status\" in the title will have a column for allocations.",
                choices: ['a', 'b'],
                choicesText: ['True', 'False'],
                correctAnswer: 0,
                rightText: "As a general rule of thumb, any report with the word \"Status\" in the title will have a column for allocations. Some examples include the Program Category Status Report and the Project Number Status Report."
            },
            {
                question: "At the top of all reports, the Start Date defaults to:",
                choices: ['a', 'b', 'c', 'd'],
                choicesText: ['The day the transaction record was created', 'The first day of the current fiscal year', 'The day the transaction took place', 'None of the above'],
                correctAnswer: 1,
                rightText: "The Start and End Date fields appear at the top of every report. These default to the start and end dates of the current fiscal year. However, they can easily be changed by clicking on the drop down menus."
            },
            {
                question: "Match the Org Code Selections with their definition:",
                subquestions: ["Shows you a Selected Org Code Rollup Report broken down into \"parent\" and \"child\" subtotals", "Shows you a total for the \"parent\" code listed in the \"Selected Org Code\" field","Shows you a Selected Org Code Rollup + Secondary Report broken down with all subtotals, including \"child\" and \"grandchild\" subtotals","Shows you a total that combines the \"parent\" code listed in the \"Selected Org Code\" field and all of the codes below it, including all \"child\" and \"grandchild\" codes",],
                choices: {
                    a: '"Selected Org Code Only"',
                    b: '"Selected Org Code Rollup"',
                    c: '"Selected Org Code Rollup + Secondary Detail"',
                    d: '"Selected Org Code + All Detail"',
                },
                correctAnswer: ["c", "a", "d", "b"],
                img: "kc_reports-question7.jpg",
                rightText: "You now understand what all of the Org Code Selection options mean.",
                wrongText: "You have matched at least one Org Code Selection option with the wrong definition. There is a cheat sheet in the Toolbox that talks about these options."
            },
            {
                question: "At a minimum, what filter(s) must you select in order to run a REGIS report?",
                choices: ['a', 'b', 'c', 'd'],
                choicesText: ['Fund Code', 'Fund Code and Org Code', 'Org Code and Program Name', 'There are no required filters'],
                correctAnswer: 0,
                img: "kc_reports-question8.jpg",
                rightText: "When determining your filters you must, at a minimum, choose a Fund Code. This is the only required filter but you may want to use others depending on your needs. Region, Org Code, BLI/PE, OCC, Program Name, Program Category, and AFC are additional filters that can be used."
            },
            {
                question: "REGIS allows you to use which character as a wildcard?",
                choices: ['a', 'b', 'c', 'd'],
                choicesText: ['%', '*', '/', 'None of the above'],
                correctAnswer: 1,
                rightText: "REGIS treats asterisks as wildcards. For example, under the travel Object Class Code 21, if you select \"21*\" as your filter, you would generate a report including detail from all travel sub-object class codes."
            },
            {
                question: "Where are totals shown on REGIS reports?",
                choices: ['a', 'b', 'c'],
                choicesText: ['Always at the top', 'Always at the bottom', 'Either the top or the bottom depending on the report'],
                correctAnswer: 2,
                rightText: "Some reports show totals at the top of the data grid while others show totals at the bottom. Most of the Summary Reports show totals at the top. All of the Detail Reports show totals at the bottom."
            },
        ];

    }
    else {
            console.log(quizId);
    }
    q2 = questions;
}