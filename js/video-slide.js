$('#close-tooltip').click(function () {
    $(this).closest("div").css("display", "none");
});

var playerElement;
var videoSource;
var page;
var alreadyComplete = false;
var coined = true;
var skippable = true;

var hasCues = false;
var onCue = false;
var videoIndex = -1;
var cueIndex = -1;

$(window).load(function () {
    page = parent.getPageInfo();
    if (page.sectionName == "Overviews" || page.sectionId == "section-intro") {
        skippable = false;
        //page.moduleName = page.moduleName.substring(1);
    }
    $("#current-section").text(page.sectionName);
    $("#current-module").text(page.moduleName);
    $("#mp4").attr("src", "../video/" + page.videoName + ".mp4");
    $("#sub-track").attr("src", "../video/" + page.videoName + ".vtt");
    videoSource = "../video/" + page.videoName + ".mp4"

    alreadyComplete = parent.moduleIsCompleted(page.sectionId, page.index);
    coined = alreadyComplete;

    if (alreadyComplete)
    {
        skippable = true;
    }

    videoIndex = getCueIndex(page.videoName);
    if (videoIndex != -1)
        hasCues = true;


    setupVideo();
});

function setupVideo() {
    var isEnforced = 0;

    $(".html5-player").each(function () {
        var id = $(this).attr("id"), wrapperID = $(this).attr("wrapper_id"), video = $(this).find("source").attr("src"),
            title = $(this).attr("video_title"), poster = $(this).attr("poster");


        var player = $(this).mediaelementplayer({
            // shows debug errors on screen
            enablePluginDebug: true,
            // Hide/Show controls when playing and mouse is not over the video
            alwaysShowControls: false,
            // rewind to beginning when media ends
            autoRewind: false,
            enableAutosize: false,
            autoplay: true,
            canSeekForward: skippable,
            // default if the <video width> is not specified
            defaultVideoWidth: 720,
            // default if the <video height> is not specified     
            defaultVideoHeight: 450,
            mode: 'auto_plugin',

            // method that fires when the Flash or Silverlight object is ready
            success: function (mediaElement, domObject) {
                var nextCheck = -1;
                mediaElement.addEventListener('ended', function (e) {
                    if (!coined) {
                        coined = true;
                        parent.makeCoin();
                    }
                }, false);
                $(".video-container").append('<div id="popup"></div>');
                $("#popup").css("display","none");
                mediaElement.addEventListener('timeupdate', function () {
                    
                    if (hasCues)
                    {
                        cueIndex = cueCheck(videoIndex, mediaElement.currentTime);
                        if (onCue && cueIndex == -1)
                            hideCue();
                        else if (!onCue && cueIndex != -1)
                            showCue();
                    }
                    if (!alreadyComplete && mediaElement.duration - mediaElement.currentTime < 10) {
                        alreadyComplete = true;
                        parent.finishedPage(page);
                    }
                    
                }, false);

                $(".show-window", parent.document).click(function () {
                    mediaElement.pause();
                });

                //mediaElement.captionsButton.off("mouseenter");


                //Setup the video and subtitle src
                //mediaElement.options.toggleCaptionsButtonWhenOnlyOne = true;
                //mediaElement.findTracks();
                //mediaElement.loadTrack(0);
                mediaElement.setSrc(videoSource);
                mediaElement.addEventListener('loadeddata', function (e) {
                    setTimeout(function () { $(".mejs__overlay-button").trigger("click"); }, 100);
                });
                playerElement = mediaElement;
            }
        });
    });
}

function hideCue()
{
    onCue = false;
    $("#popup").css("display","none");
}

function showCue(){
    onCue = true;
    $("#popup").html(cueText(videoIndex, cueIndex));
    $("#popup").css("display", "block");
}

function setComplete() {

}
