var moduleProgress = new Map();
var keyList = [];
var coinCount = 0;
var audioClip = $('<audio>').attr({
    "src": "video/coin.mp3"
});

function resetData() {
    getModuleProgress();
    for (var i = 0; i < keyList.length; i++) {
        setSuspendDataValue(keyList[i].replace("-", "_"), undefined);
    }
    keyList = [];
    console.log("reset");
    setModuleProgress();
    setSuspendDataValue("coinCount", 0);
    setSuspendDataValue("currentPage", undefined);
}

function getCoinCount() {
    coinCount = getSuspendDataValue("coinCount");
    return coinCount;
}

function addCoin() {
    coinCount += 1;
    setSuspendDataValue("coinCount", coinCount);
}

function updateCoinVisual() {
    $(".coin-counter").find("span").text("x " + coinCount);
}

function completeModule() {
    var page = getCurrentPage();
    var currentArray = moduleProgress.get(page.sectionId);
    addCoin();
    if (currentArray == undefined) {
        currentArray = [];
        keyList.push(page.sectionId);
    }
    currentArray[page.index] = true;
    moduleProgress.set(page.sectionId, currentArray);
    console.log("complete");
    setModuleProgress();
}

function updateData() {
    var completionStatus = ScormProcessGetValue("cmi.core.lesson_status");
    if (completionStatus == "not attempted") {
        ScormProcessSetValue("cmi.core.lesson_status", "incomplete");
    }

    coinCount = getSuspendDataDefault("coinCount", 0);

    getModuleProgress();
}

function quizIsComplete(quizId) {
    var page = getCurrentPage();
    return (moduleProgress.get(page.sectionId)[page.index] == true)
}

function finishedPage() {
    page = getCurrentPage();
    completeModule(page.index, page.sectionId);
    if (page.moduleId == "module-reports-check") {
        reachedEnd = true;
        ScormProcessSetValue("cmi.core.score.raw", "100");
        ScormProcessSetValue("cmi.core.lesson_status", "completed");
        SCORM_CommitData();
    }
}

// Get module progress and insert it into local map
function getModuleProgress() {
    moduleProgress = new Map();
    keyList = getSuspendDataDefault("keyList", []);
    for (var i = 0; i < keyList.length; i++) {
        moduleProgress.set(keyList[i], getSuspendDataDefault(keyList[i].replace("-", "_")));
    }
}

// Extract local map into json-able format and save it
function setModuleProgress() {
    var name;
    var outie = "";
    for (var i = 0; i < keyList.length; i++) {
        name = "" + keyList[i];
        //console.log("name" + " " + name);
        outie += name + ", ";
        name = name.replace("-", "_");
        setSuspendDataValue(name, moduleProgress.get(keyList[i]));
    }
    setSuspendDataValue("keyList", keyList);
}

//return if module in given section at given index has been completed
function moduleIsCompleted(sectionId, index) {
    var sectionSave = moduleProgress.get(sectionId);
    if (sectionSave == undefined) {
        keyList.push(sectionId);
        sectionSave = [];
        moduleProgress.set(sectionId, sectionSave);
        setModuleProgress();
    }
    return sectionSave[index] == true;
}

// Create and animate success coin
function makeCoin() {
    var img = $('<img />').attr({
        'id': 'coin',
        'src': 'img/big-coin.png',
        'alt': 'Big Coin',
        'style': "position:fixed;"
    });
    img.offset({
        top: $(window).height() / 3 - img.height() / 2,
        left: $(window).width() / 2 - img.width() / 2
    });
    $("html").append(img);

    var coinPosition = $(".icon-coinSack").offset();

    img.animate({
        height: 140,
        width: 120,
        top: "-=70",
        left: "-=60"
    }, 300, function () {
        audioClip.get(0).play();
        img.animate({
            height: 14,
            width: 12,
            top: coinPosition.top + 7,
            left: coinPosition.left + 6
        }, 500, function () {
            img.remove();
            updateCoinVisual();
        });
    });
}

function cheat() {
    var page = getCurrentPage();
    if (page.sectionId == "dashboard")
        return;
    finishedPage();
    makeCoin();
    //alert("hehe");
}

// Return true if given module has been started
function moduleIsInProgress(sectionId, index) {
    var sectionSave = moduleProgress.get(sectionId);
    if (sectionSave == undefined) {
        console.log(sectionId);
        keyList.push(sectionId); // Potentially a future error here
        sectionSave = [];
        setModuleProgress();
    }
    return sectionSave[index] == false;
}

// Change value for a module, value being if it is unattempted, incomplete, or complete
// undefined - unattempted, false - incomplete, true - complete
// Calls to this from
function changeProgressValue(sectionId, index, value) {
    var sectionSave = moduleProgress.get(sectionId);
    if (sectionSave == undefined) {
        keyList.push(sectionId);
        sectionSave = [];
    }
    if (sectionSave[index] != true)
        sectionSave[index] = value;

    moduleProgress.set(sectionId, sectionSave);
    setModuleProgress();
}

function addToInProgress(sectionId, index) {
    changeProgressValue(sectionId, index, false);
}

function getQuizSelections(quizName) {
    return getSuspendDataDefault(quizName + "Selections", [])
}

function setQuizSelections(quizName, selections) {
    setSuspendDataValue(quizName + "Selections", selections);
}

function getCurrentPage() {
    return getSuspendDataDefault("currentPage", undefined);
}

function setCurrentPage(page) {
    setSuspendDataValue("currentPage", page);
}