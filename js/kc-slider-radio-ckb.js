var quiz = $('#quiz');
var numberCorrect = 0;
var currentCorrect = false;
var page = parent.getCurrentPage();

setQuizData(page.moduleId);

var selections = parent.getQuizSelections(quizName);
var isComplete = parent.quizIsComplete(quizId);

if (isComplete)
  gradeQuiz(true);
else if (leftToAnswer() == 0)
  gradeQuiz(false);
else
  displayNext();

// Grade and display quiz results
function gradeQuiz(alreadyDone) {
  parent.setSlide(questions.length + 1, questions.length +1)
  $('.quiz-header').empty();
  numberCorrect = 0;
  var headerText = "<div class=\"pull-left ng-scope\"><p class=\"report\" style=\"font-weight: normal;\">"
  headerText += "Please scroll through your answers and review. Your grade appears at the bottom of this screen.";
  headerText += "</p></div>";
  $('.quiz-header').append(headerText);

  $('#quiz').empty();
  for (var i = 0; i < questions.length; i++) {
    currentCorrect = false;
    $('#quiz').append(createQuestionElement(i, true));
  }

  var footerFill = "";
  footerFill += '<div class="col-sm-12"><div class="pull-left"><p class="report" style="margin:0;">'
  footerFill += ' You answered ' + numberCorrect + ' correctly out of ' + questions.length + " for a score of " + getScore() + "%</p>";

  if (getScore() >= passingScore) {
    if (!alreadyDone) {
      parent.finishedPage();
      parent.makeCoin();
      console.log("coined");
    }

    footerFill += 'You have passed! You may continue on to the next lesson.</div></div>';
  }
  else {
    footerFill += 'You have failed to score above an ' + passingScore + '% and must retake the Overview Module.'
      + ' <button class = "button-link retake-overview" style="text-decoration: underline" onclick="resetScore()">Click here to retake the Overview.</button></div></div>';
  }

  $('.quiz-footer').empty();
  $('.quiz-footer').append(footerFill);
}

// Reset quiz data
function resetScore() {
  selections = [];
  parent.setQuizSelections(quizName, [])
  parent.loadOverview(videoName, quizName);
}

// Get score percent. just looks cleaner
function getScore() {
  return ((numberCorrect / questions.length * 100)) | 0;
}

// Return number of questions left to answer
function leftToAnswer() {
  i
  var leftToAnswer = 0;
  for (var i = 0; i < questions.length; i++) {
    if (selections[i] == undefined)
      leftToAnswer++;
  }
  if (leftToAnswer == 0)
    $('#grade').removeClass('btn-disabled');
  return leftToAnswer;
}

// Click handler for the 'next' button
$('#next').on('click', function (e) {
  e.preventDefault();

  // Suspend click listener during fade animation
  if (quiz.is(':animated')) {
    return false;
  }

  if (selections[questionCounter] instanceof Array) {
    if (selections[questionCounter].indexOf('0') > -1) {
    } else {
      questions[questionCounter].answered = true;
    }
    questionCounter++;
    displayNext();
  } else {
    // If no user selection, progress is stopped
    if (isNaN(selections[questionCounter])) {
      //alert('Please make a selection!');
    } else {
      questions[questionCounter].answered = true;
    } if (questionCounter === questions.length - 1) {
      return false;
    }
    questionCounter++;

    displayNext();
  }

});

$('#grade').on('click', function (e) {
  if ($(this).hasClass("btn-disabled"))
    return;

  gradeQuiz(false);
});


// Click handler for the 'prev' button
$('#prev').on('click', function (e) {
  e.preventDefault();

  if (quiz.is(':animated')) {
    return false;
  }
  if (questionCounter === 0)
    return false;

  questionCounter--;
  displayNext();
});

// Click handler for the 'Start Over' button
$('#start').on('click', function (e) {
  e.preventDefault();

  if (quiz.is(':animated')) {
    return false;
  }
  questionCounter = 0;
  selections = [];
  displayNext();
  $('#start').hide();
});

// Animates buttons on hover
$('.button').on('mouseenter', function () {
  $(this).addClass('active');
});
$('.button').on('mouseleave', function () {
  $(this).removeClass('active');
});


// Creates and returns the div that contains the questions and the answer selections
function createQuestionElement(index, graded) {
  if (!graded)
  {
    parent.setSlide(index+1, questions.length + 1);
  }
  var outerElement;
  if (graded && index != questions.length - 1)
    outerElement = $('<li>', {
      class: 'graded'
    });
  else if (graded)
    outerElement = $('<li>', {
      class: 'graded',
      style: 'border-width: 0px'
    });
  else
    outerElement = $('<li>');

  var rowElement = $('<div>', {
    class: 'row'
  })

  var qElement = $('<div>', {
    id: 'question',
    class: 'col-sm-12 ng-scope'
  });

  qElement.css("padding", "0 30px");
  var header = $('<div class = \"indent ng-binding\">' + (index + 1) + ':</div>');
  qElement.append(header);
  var checboxAndRadiosQuestions;
  var question = $('<div class = \"question-html\">').append(questions[index].question);

  if (questions[index].hasOwnProperty('subquestions')) {
    checboxAndRadiosQuestions = createCheckboxChoices(index, graded);
    question.append(checboxAndRadiosQuestions);
    qElement.append(question);
    qElement.append(createCheckboxAnswers(index, graded));
  } else {
    checboxAndRadiosQuestions = createRadios(index, graded);
    qElement.append(question);
    qElement.append(checboxAndRadiosQuestions);
  }

  if (questions[index].img != undefined && !graded)
    qElement.append('<div class="quiz-image"><img src="../img/' + questions[index].img + '"></div>');
  qElement.append("</div></div>")
  rowElement.append(qElement);
  outerElement.append(rowElement);
  return outerElement;
}

//Creates a single radio button and text
function createOneRadio(i, qIndex, graded) {
  var input = '';
  input = '<li><div class = "';

  if (!graded)
    input += 'row"><div class="col-sm-12"><div class = \"radio ng-scope\">';

  else if (graded) {
    input += "answer";
    if (i == selections[qIndex]) {
      if (selections[qIndex] == questions[qIndex].correctAnswer) {
        input += ' answer-correct-selected">';
        numberCorrect++;
        currentCorrect = true;
      }
      else
        input += ' answer-wrong-selected">';
    }
    else
      input += '">';
  }

  input += "<label style=\"font-weight: normal;\" title=\"" + questions[qIndex].choicesText[i] + "\">";
  if (!graded)
    input += "<input style=\"margin-top: 4px;\" class=\"question-radio ng-pristine ng-valid\" name=\"answer\" value=\""
      + i + "\" type=\"radio\">";

  input += "<span class=\"ng-binding\">" + questions[qIndex].choices[i] + ". " + questions[qIndex].choicesText[i] + "</span></label></div>";
  if (!graded)
    input += '</div></div>';
  return input;
}

//Create all radio buttons
function createRadios(index, graded) {
  var radioList = $('<ol class="radio-questions answers list-unstyled">');
  var item;
  var input = '';
  for (var i = 0; i < questions[index].choices.length; i++) {
    radioList.append(createOneRadio(i, index, graded));
    radioList.append(item);
  }

  if (graded) {
    var gradeText = questions[index].rightText;
    if (currentCorrect)
      startText = "Correct! ";
    else {
      startText = "Sorry. That is Incorrect. ";
      if (questions[index].wrongText != undefined)
        gradeText = questions[index].wrongText;
    }

    if (index == 3 && quizName == "REPORTS") {
      startText = "<div class = 'popup-80'>" + startText;
      gradeText += "</div><img src = '../img/kc_reports-question4-popup.jpg'>"
    }
    radioList.append('<div class = "well answer-correct-not-selected answer-popup">' + startText + gradeText + '</div>');
  }

  return radioList;
}

// Creates a list of the answer choices
function createCheckboxChoices(index, graded) {
  var choicesListArea = $('<ol class="choicesList question-matching list-alpha">');
  var choicesList = "";
  for (key in questions[index].choices)
    choicesList += '<li>' + questions[index].choices[key] + '</li>';

  choicesListArea.append(choicesList);
  return choicesListArea;
}

//Creates a list of the answer inputs. Adds graded text if quiz is graded
function createCheckboxAnswers(index, graded) {
  var answerListArea = $('<ol class="choiceQuestionList answers list-unstyled">');
  var selectAnswerList = '<select name="answer" class="select-answer">';
  var answerList = "";

  for (key in questions[index].choices)
    selectAnswerList += '<option value="' + key + '">' + key + '</option>';

  selectAnswerList += '</select>';

  //console.log(index);
  //console.log(questions[index].question);
  //console.log(questions[index].subquestions.length);
  for (i = 0; i < questions[index].subquestions.length; i++) {
    var count = i + 1;
    answerList += '<li><div class="row"><div class="col-sm-12' + ((graded) ? answerClass(index, i) : "") + '" style="padding:0;"><div class="matching"><div class="indent">'
      + ((!graded) ? count + ')' : selectedAnswer(selections[index][i])) + '</div>'
      + ((!graded) ? selectAnswerList : "") + '<div style="display:inline-block;width:75%;">'+questions[index].subquestions[i] + '</div></div></div></div></li>';
  }

  if (graded) {
    var startText;
    var gradeText = questions[index].rightText;
    //console.log(selections[index] + " " + questions[index].correctAnswer)
    if (isEqual(selections[index], questions[index].correctAnswer)) {
      numberCorrect++;
      currentCorrect = true;
    }
    if (currentCorrect)
      startText = "Correct! ";
    else
    {
      startText = "Sorry. That is Incorrect. ";
      gradeText = questions[index].wrongText;
    }

    answerList += '<div class = "well answer-correct-not-selected answer-popup">' + startText + gradeText + '</div>';
  }
  answerList += '</ol>';
  answerListArea.append(answerList);
  return answerListArea;
}

//helper function for checkbox label creation
function selectedAnswer(selection) {
  if (selection != undefined)
    return "(" + selection + ")";

  return "()";
}

//get class for checkbox grading
function answerClass(index, i) {
  //console.log(selections[index][i] + " " + questions[index].correctAnswer[i]);

  if (selections[index][i] == questions[index].correctAnswer[i])
    return " answer answer-correct-selected";

  return " answer answer-wrong-selected";
}

// Reads the user selection and pushes the value to an array
function choose() {
  if ($("#quiz ol").hasClass("radio-questions")) {
    selections[questionCounter] = +$('input[name="answer"]:checked').val();
  } else {
    var selectedAnswers = [];
    $(".choiceQuestionList select").each(function () {
      var selectedAnswerValue = $(this).find("option").filter(':selected').val();
      selectedAnswers.push(selectedAnswerValue);
      selections[questionCounter] = selectedAnswers;
    });
  }
  parent.setQuizSelections(quizName, selections);
}

// Setups input functionality
function setupInputs() {
  var selectedAnswers = [];

  // Sets values of input fields to saved values
  $(".choiceQuestionList select").each(function (index) {
    if (selections[questionCounter] == undefined)
      return;
    $(this).val(selections[questionCounter][index]);
  });

  // Save quiz selections when inputs are changed for dropdown question
  $('.select-answer').change(function () {
    $(".choiceQuestionList select").each(function (index) {
      var selectedAnswerValue = $(this).find("option").filter(':selected').val();
      selectedAnswers[index] = (selectedAnswerValue);
      $('#questions-left').text(leftToAnswer() + " Questions Left");
      selections[questionCounter] = selectedAnswers;
    });
    parent.setQuizSelections(quizName, selections);
  })

  // Save quiz selections when inputs are changed for radio buttons
  $('.question-radio').click(function () {
    selections[questionCounter] = $(this).val();
    $('#questions-left').text(leftToAnswer() + " Questions Left");
    parent.setQuizSelections(quizName, selections);
  });
}

// Displays next question
function displayNext() {
  quiz.fadeOut(50, function () {
    $('#question').remove();
    $('#quiz').empty();

    if (questionCounter < questions.length) {
      $('#questions-left').text(leftToAnswer() + " Questions Left");
      var nextQuestion = createQuestionElement(questionCounter, false);
      quiz.append(nextQuestion).fadeIn(150);

      setupInputs();

      if (!(isNaN(selections[questionCounter]))) {
        $('input[value=' + selections[questionCounter] + ']').prop('checked', true);
      }

      // Controls display of 'prev' button
      if (questionCounter === 1) {
        $('#prev').removeClass('btn-disabled');
        $('#next').removeClass('btn-disabled');
      } else if (questionCounter === 0) {

        $('#prev').addClass('btn-disabled');
        $('#next').removeClass('btn-disabled');
      }
      else if (questionCounter === questions.length - 1) {
        $('#next').addClass('btn-disabled');
      }
      else {

        $('#next').removeClass('btn-disabled');
      }
    }
  });
}

// Computes score and returns a paragraph element to be displayed
function displayScore() {
  var score = $('<p>', { id: 'question' });

  var numCorrect = 0;
  for (var i = 0; i < selections.length; i++) {
    if (selections[i] === questions[i].correctAnswer) {
      numCorrect++;
    } else {
      if (isEqual(selections[i], questions[i].correctAnswer)) {
        numCorrect++;
      }
    }
  }

  score.append('You got ' + numCorrect + ' questions out of ' +
    questions.length + ' right!!!');
  return score;
}


//https://gomakethings.com/check-if-two-arrays-or-objects-are-equal-with-javascript/
function isEqual(value, other) {
  //var isEqual = function (value, other) {

  // Get the value type
  var type = Object.prototype.toString.call(value);

  // If the two objects are not the same type, return false
  if (type !== Object.prototype.toString.call(other)) return false;

  // If items are not an object or array, return false
  if (['[object Array]', '[object Object]'].indexOf(type) < 0) return false;

  // Compare the length of the length of the two items
  var valueLen = type === '[object Array]' ? value.length : Object.keys(value).length;
  var otherLen = type === '[object Array]' ? other.length : Object.keys(other).length;
  if (valueLen !== otherLen) return false;

  // Compare two items
  var compare = function (item1, item2) {

    // Get the object type
    var itemType = Object.prototype.toString.call(item1);

    // If an object or array, compare recursively
    if (['[object Array]', '[object Object]'].indexOf(itemType) >= 0) {
      if (!isEqual(item1, item2)) return false;
    }

    // Otherwise, do a simple comparison
    else {

      // If the two items are not the same type, return false
      if (itemType !== Object.prototype.toString.call(item2)) return false;

      // Else if it's a function, convert to a string and compare
      // Otherwise, just compare
      if (itemType === '[object Function]') {
        if (item1.toString() !== item2.toString()) return false;
      } else {
        if (item1 !== item2) return false;
      }

    }
  };

  // Compare properties
  if (type === '[object Array]') {
    for (var i = 0; i < valueLen; i++) {
      if (compare(value[i], other[i]) === false) return false;
    }
  } else {
    for (var key in value) {
      if (value.hasOwnProperty(key)) {
        if (compare(value[key], other[key]) === false) return false;
      }
    }
  }

  // If nothing failed, return true
  return true;

};