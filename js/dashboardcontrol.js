Array.prototype.includes = function (value) {
    return this.indexOf(value) > -1 || !1
}

$(window).on('load', function () {
    startDash();
    $('.tooltip').hover(
        function () {
            {
                // Display orange "must complete X module to unluck" tooltip
                if ($(this).hasClass("locked")) {
                    $('#lock-message-box').css("display", "block");
                    $('#lock-message-box').css("top", $(this).offset().top);
                    $('#lock-message-box').css("left", $(this).offset().left + 100);
                    $('#lock-message-box p').empty();

                    var filled = false;
                    var classList = $(this).attr('class').split(/\s+/);
                    
                    // Set up tooltips
                    $.each(classList, function (index, item) {
                        // Find class name of module required for $this module, then get the name of the module
                        if (item.indexOf("module") == 0) {
                            {
                                var name = $("#" + item + " a").text();
                                if (name == "Knowledge Check")
                                    name = $("#" + item).attr("data-name");
                                $('#lock-message-box p').append(tooltipText(name));
                                filled = true;
                            }
                        }
                    });
                    if (!filled) {
                        var parentClasses = $(this).closest("div").attr('class').split(/\s+/);
                        $.each(parentClasses, function (index, item) {
                            if (item.indexOf("module") == 0) {
                                {
                                    var name = $("#" + item + " a").text();
                                    if (name == "Knowledge Check")
                                        name = $("#" + item).attr("data-name");
                                    $('#lock-message-box p').empty();
                                    $('#lock-message-box p').append(tooltipText(name));
                                    filled = true;
                                }
                            }
                        });
                    }

                    if ($(this).attr("id") == "module-regis-overview")
                    {
                        $('#lock-message-box p').empty();
                        $('#lock-message-box p').append(tooltipTextAnd("Welcome to the REGIS VLE","Navigating the Regis VLE"));
                    }

                }
            }
        }, function () {
            $('#lock-message-box').css("display", "none");
        }
    );
});

// Setup Dashboard
function startDash() {

    lockAll();

    // If a module is completed and a section requires it, unlock that section
    $("li.prereq").each(function () {
        var sectionId = $(this).closest(".module-container").attr("id");
        if (parent.moduleIsCompleted(sectionId, $(this).index()))
            $(".requires-module." + $(this).attr("id")).removeClass("locked");
        
        if (!parent.moduleIsCompleted("section-intro",0))
            $("#section-overviews").addClass("locked");

        console.log("test");

    });

    // Go through each unlocked section and set icons to save values
    $(".module-container").each(function () {
        var sectionName = $(this).find(".module-title").text();
        var sectionId = $(this).attr("id");
        if (!$(this).hasClass("locked")) {
            $(this).find("li").each(function (index) {

                $(this).find("i").removeClass("status-icon-lock")
                $(this).removeClass("locked");
                if (parent.moduleIsCompleted(sectionId, index))
                    $(this).find("i").addClass("status-icon-complete");
                else if (parent.moduleIsInProgress(sectionId, index))
                    $(this).find("i").addClass("status-icon-incomplete");
                else
                    $(this).find("i").addClass("status-icon-not-complete");

                var modId = $(this).attr("id");

                $(this).find("a").click(function () {
                    var title = $(this).text();
                    var videoName = $(this).attr("data-video");
                    parent.addToInProgress(sectionId, index);
                    parent.loadPage(sectionId, sectionName, title, videoName, index, modId);
                });
            });
        }
    });

    // Modules are default set to not complete when their section is unlocked. 
    // Modules in the first section need to be set to locked if their prerequisite module isn't complete
    $("li.prereq").each(function () {
        var sectionId = $(this).closest(".module-container").attr("id");
        if (!parent.moduleIsCompleted(sectionId, $(this).index())) {
            $("li.requires-module." + $(this).attr("id")).each(function () {
                if ($(this).closest(".module-container").hasClass("locked"))
                    return true;
                $(this).find("a").css("text-decoration", "none");
                $(this).find("a").css("color", "#333");
                $(this).addClass("locked");
                $(this).find("i").removeClass("status-icon-not-complete");
                $(this).find("i").addClass("status-icon-lock");
                $(this).find("a").off("click");
            });
        }
    });
}

// Locks all modules/sections
function lockAll() {
    $(".module-container li.tooltip").addClass("locked");
    $(".module-container").addClass("locked");
    $("#section-intro").removeClass("locked");
    $("#module-admin").removeClass("locked");
}


function tooltipText(required) {
    return "You must complete <b>" + required + "</b> to unlock this module";
}

function tooltipTextAnd(required1,required2)
{
    return "You must complete <b>" + required1 + "</b> and <b>" + required2+"</b> to unlock this module";
}