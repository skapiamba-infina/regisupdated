jQuery(document).ready(function($) {
	var url = window.location.pathname;
	var currentPage = url.substring(url.lastIndexOf('/')+1);
	var sessionKey1 = currentPage + ".videoCompleted";
	var isCompleted = sessionStorage.getItem(sessionKey1);
	var isEnforced = 0;

	if(!isCompleted){
		isCompleted = 0;
	}

	function checkNextBtn() {
		/*
		 * if(ttlRequired > 0) { if(Math.pow(2, ttlRequired) - 1 == ttlRead) { var complete = sessionStorage.getItem(sessionKey2);
		 * 
		 * if(!complete){ complete = 0; }
		 * 
		 * if(complete > 0) { $(".next-btn").removeClass("disabled"); } } }
		 */
		var complete = parseInt(sessionStorage.getItem(sessionKey1));

		if(!complete){
			complete = 0;
		}

		if(complete > 0) {
			$(".next-btn").removeClass("disabled");
		}
	}

	$(".html5-player").each(function(){
			
		var id = $(this).attr("id"), wrapperID = $(this).attr("wrapper_id"), video = $(this).find("source").attr("src"),
		autoplay = $(this).attr("autoplay"), poster = $(this).attr("poster")
		enforced = $(this).attr("enforced");

		//console.log(wrapperID);
		if(enforced != undefined && enforced != 0) {
			isEnforced++;
			$(".next-btn").addClass("disabled");
		}

		checkNextBtn();

		// if(video && wrapperID)
		// {   


			        	var player = $(this).mediaelementplayer({
			                    // shows debug errors on screen
			                        enablePluginDebug: true,
			    			// Hide/Show controls when playing and mouse is not over the video
						alwaysShowControls: false,
						// rewind to beginning when media ends
					        autoRewind: false,
			                        enableAutosize: false,
			                        // default if the <video width> is not specified
			                        defaultVideoWidth: 720,
			                        // default if the <video height> is not specified     
			                        defaultVideoHeight: 450,
			                        mode: 'auto_plugin',
			                        // method that fires when the Flash or Silverlight object is ready
			                        success: function (mediaElement, domObject) { 
			                            //console.log('on success');
			                            //console.log(mediaElement);
			                            // add event listener
			                            //mediaElement.addEventListener('timeupdate', function(e) {
			                                //document.getElementById('current-time').innerHTML = mediaElement.currentTime;

			                            //}, false);
			                            //
			                            mediaElement.addEventListener('ended', function(e) {
			                                //console.log('Video has ended!');
			                                if(enforced) {
			                                    isEnforced--;
			                                    if(isEnforced == 0) {
			                                            // $(".next-btn").removeClass("disabled");
			                                            sessionStorage.setItem(sessionKey1, 1);
			                                    }
			                                    checkNextBtn();
			                                }
			                            }, false);
			                            
			                            mediaElement.addEventListener('loadeddata', function(e) {
			                                //console.log("loaded");
			                                // mediaElement.play();
			                            });
			                            // call the play method
			                            //mediaElement.pause();
			                            //mediaElement.play();
			                        }
			    		});




		// 	$(this).mediaelementplayer({
		// 		// Hide/Show controls when playing and mouse is not over the video
		// 		alwaysShowControls: false,
		// 		// rewind to beginning when media ends
		//         autoRewind: false				
		// 	});
		// 	$('video').on('ended',function(){
		// 		//console.log('Video has ended!');
		// 		if(enforced) {
		// 			isEnforced--;
		// 			if(isEnforced == 0) {
		// 				// $(".next-btn").removeClass("disabled");
		// 				sessionStorage.setItem(sessionKey1, 1);
		// 			}
		// 			checkNextBtn();
		// 		}
		// 	});

		// }
	});
	
});
