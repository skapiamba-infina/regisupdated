var allCues = [
    {
        videoName: "trans_advanced_filter",
        cues: [
            {
                startTime: "00:13:26",
                endTime: "00:13:33",
                text: "Scenario Templates - Suggested Columns to Add",
                link: ".doc"
            },
        ]
    },
    {
        videoName: "trans_advanced",
        cues: [
            {
                startTime: "00:00:37",
                endTime: "00:00:51",
                text: "PCPS was replaced with the PRISM PCard Module in March 2017.",
                link: "Replace"
            },
            {
                startTime: "00:01:01",
                endTime: "00:01:20",
                text: "PCPS was replaced with the PRISM PCard Module in March 2017.",
                link: "Replace"
            },
            {
                startTime: "00:01:55",
                endTime: "00:02:14",
                text: "PCPS was replaced with the PRISM PCard Module in March 2017.",
                link: "Replace"
            },
            {
                startTime: "00:05:27",
                endTime: "00:05:57",
                text: "PCPS was replaced with the PRISM PCard Module in March 2017.",
                link: "Replace"
            },
            {
                startTime: "00:07:37",
                endTime: "00:08:17",
                text: "GovTrip was replaced with E2 in July 2015.",
                link: "Replace"
            },
        ]
    },
    {
        videoName: "trans_blanket",
        cues: [
            {
                startTime: "00:00:20",
                endTime: "00:00:35",
                text: "PCPS was replaced with the PRISM PCard Module in March 2017.",
                link: "Replace"
            },
            {
                startTime: "00:00:57",
                endTime: "00:01:07",
                text: "PCPS was replaced with the PRISM PCard Module in March 2017.",
                link: "Replace"
            },
            {
                startTime: "00:06:53",
                endTime: "00:07:03",
                text: "PCPS was replaced with the PRISM PCard Module in March 2017.",
                link: "Replace"
            },
        ]
    },
    {
        videoName: "trans_delphi_cross",
        cues: [
            {
                startTime: "00:02:13",
                endTime: "00:02:20",
                text: "DELPHI Column Headers",
                link: ".doc"
            },
        ]
    },
    {
        videoName: "trans_fund_cert",
        cues: [
            {
                startTime: "00:01:40",
                endTime: "00:01:46",
                text: "GovTrip was replaced with E2 in July 2015.",
                link: "Replace"
            },
        ]
    },
    {
        videoName: "trans_trans_regis",
        cues: [
            {
                startTime: "00:02:28",
                endTime: "00:02:34",
                text: "REGIS Column Headers",
                link: ".doc"
            },
            {
                startTime: "00:08:08",
                endTime: "00:08:18",
                text: "PCPS was replaced with the PRISM PCard Module in March 2017.",
                link: "Replace"
            },
        ]
    },
    {
        videoName: "reports_trans",
        cues: [
            {
                startTime: "00:07:00",
                endTime: "00:07:06",
                text: "REGIS Column Headers",
                link: ".doc"
            },
        ]
    },
    {
        videoName: "trans_overview",
        cues: [
            {
                startTime: "00:14:09",
                endTime: "00:14:15",
                text: "REGIS Field Dependencies",
                link: ".doc"
            },
            {
                startTime: "00:23:44",
                endTime: "00:23:50",
                text: "REGIS Column Headers",
                link: ".doc"
            },
            {
                startTime: "00:18:18",
                endTime: "00:18:27",
                text: "PCPS was replaced with the PRISM PCard Module in March 2017.",
                link: "Replace"
            },
            {
                startTime: "00:29:25",
                endTime: "00:29:35",
                text: "PCPS was replaced with the PRISM PCard Module in March 2017.",
                link: "Replace"
            },
        ]
    },
    {
        videoName: "buget_essentials",
        cues: [
            {
                startTime: "00:26:27",
                endTime: "00:26:41",
                text: "Cross Reference of DELPHI Accounting String Data Elements with REGIS and Other Systems",
                link: ".doc"
            },
            {
                startTime: "00:26:21",
                endTime: "00:26:24",
                text: "DELPHI Seven Data Elements",
                link: ".pptx"
            },
            {
                startTime: "00:25:51",
                endTime: "00:25:53",
                text: "Fund Codes and Limiters",
                link: ".doc"
            },
            {
                startTime: "00:27:21",
                endTime: "00:27:25",
                text: "DELPHI Column Headers",
                link: ".doc",
                text2: "REGIS Column Headers",
                link2: ".doc"
            },
            {
                startTime: "00:04:46",
                endTime: "00:04:56",
                text: "PCPS was replaced with the PRISM PCard Module in March 2017.",
                link: "Replace"
            },
            {
                startTime: "00:04:31",
                endTime: "00:04:40",
                text: "GovTrip was replaced with E2 in July 2015.",
                link: "Replace"
            },
        ]
    },
    {
        videoName: "allocations_overview",
        cues: [
            {
                startTime: "00:16:15",
                endTime: "00:16:20",
                text: "Actual Allocations Window Origin Column Letters",
                link: ".doc"
            },
        ]
    },
    {
        videoName: "buget_essentials",
        cues: [
            {
                startTime: "00:13:27",
                endTime: "00:13:31",
                text: "Funding Sources",
                link: ".doc"
            },
        ]
    },
    {
        videoName: "trans_reconciliation",
        cues: [
            {
                startTime: "00:14:10",
                endTime: "00:14:20",
                text: "PCPS was replaced with the PRISM PCard Module in March 2017.",
                link: "Replace"
            },
        ]
    },
    {
        videoName: "welcome",
        cues: [
            {
                startTime: "00:00:40",
                endTime: "00:00:42",
                text: "PCPS was replaced with the PRISM PCard Module in March 2017.",
                link: "Replace"
            },
            {
                startTime: "00:00:43",
                endTime: "00:00:46",
                text: "GovTrip was replaced with E2 in July 2015.",
                link: "Replace"
            },
        ]

    }
];

function getCueIndex(video) {
    for (var i = 0; i < allCues.length; i++)
        if (video == allCues[i].videoName)
            return i;
    return -1;
}

function strToNum(str) {
    return str.substring(3, 5) * 60 + str.substring(6, 8) / 1;
}

function cueCheck(index, time) {
    for (var i = 0; i < allCues[index].cues.length; i++) {
        if (time > strToNum(allCues[index].cues[i].startTime) && time < strToNum(allCues[index].cues[i].endTime))
            return i;
    }

    return -1;
}

function cueText(index, cueNumber) {
    var outText = '<br><a href = "../toolbox/' + allCues[index].cues[cueNumber].text + allCues[index].cues[cueNumber].link + '">' + allCues[index].cues[cueNumber].text + '</a>';

    if (allCues[index].cues[cueNumber].link == "Replace")
        return allCues[index].cues[cueNumber].text;

    if (allCues[index].cues[cueNumber].text2 != null) {
        outText += '<br><a href = "../toolbox/' + allCues[index].cues[cueNumber].text2 + allCues[index].cues[cueNumber].link2 + '">' + allCues[index].cues[cueNumber].text2 + '</a>';
        outText = 'Open the links below in a new window: ' + outText;
    }
    else
        outText = 'Open the below link in a new window: ' + outText;

    
    return outText;
}