function initSuspendData() {
    var mainObj = {};
    if (isElms())
        getSuspendedData = ScormProcessGetValue('cmi.suspend_data');
    else
        getSuspendedData = localStorage.getItem("suspendData");
    //getSuspendedData = localStorage.getItem("suspendData");

    if (getSuspendedData == '' || getSuspendedData == null) {
        var defaultSuspendData = JSON.stringify(mainObj);
        if (isElms())
            ScormProcessSetValue('cmi.suspend_data', defaultSuspendData);
        else
            localStorage.setItem("suspendData", defaultSuspendData);
    }
}

//Returns stored JSON data, initializes the JSON save with given default value if it doesn't exist
function getSuspendDataDefault(strObjProperty, defaultValue) {
    var value = getSuspendDataValue(strObjProperty);
    //console.log(strObjProperty + " " + value)
    if (value == undefined) {
        value = defaultValue;
        setSuspendDataValue(strObjProperty, defaultValue);
    }
    return value;
}

//Returns a Json Object, Array, Number or String Depending on your values
function getSuspendDataValue(strObjProperty) {

    var getSuspendedData;

    if (isElms())
        getSuspendedData = ScormProcessGetValue('cmi.suspend_data');
    else
        getSuspendedData = localStorage.getItem("suspendData");

    var mainObj = JSON.parse(getSuspendedData);

    var testText1 = strObjProperty.substring(0, 7);

    if (!(testText1 == 'mainObj')) {
        strObjProperty = 'mainObj.' + strObjProperty;
    }

    return eval(strObjProperty);
}

function resetSuspendData() {
    var mainObj = {};
    var defaultSuspendData = JSON.stringify(mainObj);
    localStorage.setItem("suspendData", defaultSuspendData);
}

function setSuspendDataValue(strObjProperty, objValue) {
    var getSuspendedData;
    if (isElms())
        getSuspendedData = ScormProcessGetValue('cmi.suspend_data');
    else
        getSuspendedData = localStorage.getItem("suspendData");

    var mainObj = JSON.parse(getSuspendedData);

    switch (typeof objValue) {
        case "string":
            var setObjVal = 'mainObj.' + strObjProperty + ' = "' + objValue + '";';
            break;
        case "object":
            objValue = JSON.stringify(objValue);
            //alert(objValue);
            var setObjVal = 'mainObj.' + strObjProperty + ' = ' + objValue + ';';
            //console.log(setObjVal);
            break;
        default:
            var setObjVal = 'mainObj.' + strObjProperty + ' = ' + objValue + ';';
            //console.log(setObjVal);
            break;
    }

    eval(setObjVal);
    mainObj = JSON.stringify(mainObj);

    if (isElms()) {
        ScormProcessSetValue('cmi.suspend_data', mainObj);
        SCORM_CommitData();
    }
    else
        localStorage.setItem("suspendData", mainObj);
}