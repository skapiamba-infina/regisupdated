# SCORM

This is a repository for core functionality of scorm projects.

## Changelog
### V1.1
Release Date - 25 Jul 2016

* Restructure the template file system
	* Only APIs, scormnavengine, and Jquery Lib should be in parent
* This has the Branching Features within the sco objects
* Sidebar Menus Updated
* BookMark Working 

## JS File Refences 
##### launchpage html JS Files
* scormdriver.js, scormfunctions.js: All of these are just scorm 1.2 libraries. All of these communicates with window.parent.API. 
	The elms environment attaches this API when the admin designate the page as the launcher.
* scormnavengine.js - This file is responsible for launching the after the launcher is called. 
	This communicates directly to scorm libraries.
* storedata.js - Allows you to create your custom variables with values
```javascript
//Sample Usage
var strObj = 'test';
var arryObj = ["1","2","3"];
var objObj = {
    "glossary": {
        "title": "example glossary",
		"GlossDiv": {
            "title": "S",
			"GlossList": {
                "GlossEntry": {
                    "ID": "SGML",
					"SortAs": "SGML",
					"GlossTerm": "Standard Generalized Markup Language",
					"Acronym": "SGML",
					"Abbrev": "ISO 8879:1986",
					"GlossDef": {
                        "para": "A meta-markup language, used to create markup languages such as DocBook.",
						"GlossSeeAlso": ["GML", "XML"]
                    },
					"GlossSee": "markup"
                }
            }
        }
    }
};


parent.setSuspendDataValue ('strObj', strObj);
parent.setSuspendDataValue ('arryObj', arryObj);
parent.setSuspendDataValue ('objObj', objObj);
alert(JSON.stringify(parent.getSuspendDataValue ('mainObj')));
```
						  
##### slides HTML JS Files
* jquery.js -  is a jQuery v1.11.3 library
* bootstrap.js - for bootstrap template
* include.js - loads the files inside the include folder
* global.js
	* Side Features: Slides names of the slides, course completion, and setting the bookmark
* scormnav.js - responsible for disable/enabling next button, disable/enable the sidebar menu, display's the warning message for the next button.
	* This communicates with scormnavengine.js
* sub-pages.js -  Allows the Regular Page to Branch out. See Read Me inside the file
* sub-pages-nav-8-1.js - Allows the tracking of sub pages.(Still Adding Features) See Read Me inside the file 
			
## JS Function Refences  
##### Scorm Functions
* window.parent.API. - enter this in the console to check what native scorm functions are supported
 **To use this native scorm functions. Read This: http://scorm.com/scorm-explained/technical-scorm/run-time/run-time-reference/
* APIWrapper.js, scormdriver.js, scormfunctions.js: These libraries communicates directly to API. Read the FAA Documentation for the Runtime environment	

    	
## Usage 

##### File Structure
css
fonts
img
js
audio
img
include
resources
slides
video
launchpage.html

##### Inside each slides 
coursename/slides/slide.html

* insert this in head section
```html
 <link href="../css/bootstrap.min.css" rel="stylesheet">
 <link href="../css/global.css" rel="stylesheet">
 <link href="../css/course.css" rel="stylesheet">
```
* insert this in footer before the closing body tag 
```html
<script src="../js/jquery.js"></script>
<script src="../js/bootstrap.js"></script>
<script src="../js/include.js"></script>
<script src="../js/global.js"></script>
<!-- scripts.js -->
<script src="../js/scormnav.js" type="text/javascript"></script>
```
Note: You can add your custom scripts after these scripts above

##### Inside scormnavengine.js
* Update the list of pageArray based on your slides
* Update the list of bookmarkArray 
```javascript
var pageArray = new Array(2);
pageArray[0] = "slides/slide-1.html";
pageArray[1] = "slides/slide-3.html";

var bookmarkArray  = new Array(2);
bookmarkArray[0] = "slide-1.html";
bookmarkArray[1] = "slide-2.html";
```


##### Inside slide-1.html
* Insert this html elements after the body tag
```html
<body>
  <div class="spec-hidden">
  	<a href="#" onclick="parent.gotoPage508(0);">Plain text version.</a>
</div>
```
##### Inside slide-508.html
* Insert at the end of the page
```html
<p><input name="Complete" value="Complete" onclick="parent.gotoPage508(1);" type="button"></p>
```
##### Inside menu.html 
* Replace and Update the list

* Insert id in the ol
```html
<ol id="sidebar-menu">
```
* Insert Scripts below ol
```javascript
	enableSidebarMenu();
 
    //collapseableMenu(intStartLi, intCount, strIDClass, intSubMenuCount, strLinkTitle)
    collapseableMenu(5, 12, '.item-4', 1, 'Custom Title');		// Module 1: Lesson 1   
```

* Insert style after  ol and scripts
```html
<style>
#menu ol li a.disabled-link {
    color: #626c7b;
}
</style>
```
* Note: For more detail see menu.html


##### For Any slide.html that is required to complete

* Insert body class
```html
<body class="required">
```

##### Branching Sub Pages

###### Main Page of the Branches 
1. Create the branch slide.html (It should be part of var pageArray inside scormnavengine.js).
2. Add this sub-pages.js (script) at the bottom of the Main Page of the  subpage/s
3. Replace subPage array from Main Page

###### Branch pages
1. Add this sub-pages-nav-#-#.js (script) at the bottom of the each subpage. Make sure to remage the filename accordingly.
2. Replace subPage array inside sub-pages-nav-#-#.js
3. Call the subPageComplete on the last slide of branch pages 
4. Replace or Modify slideName variable inside sub-pages-nav-#-#.js
5. Add a required-sub for all require pages inside sub-pages-nav-#-#.js 


##### Saving Your Data/Input into Scorm
```javascript
parent.setSuspendDataValue(strObjProperty, objValue);
//strObjProperty - Name of Your Variable
//objValue - Can be String, Number, Array, or Json
	
//Returns a Json Object, Array, Number or String Depending on your values when you set your data
parent.getSuspendDataValue(strObjProperty);

//Note: Make sure to check if your variable name already exist, so you will not overwrite other developers's variables.
parent.getSuspendDataValue ('mainObj'); 
```

##### Creating XML File
* View youtube videos
https://www.youtube.com/watch?v=-0HxfdOwFSI

* Important text to insert into Metadata
```xml
General > Title > Your Title
General > Catalog > Your Title 
General > Catalog Entry > Your Title Number
General > Description > Your Title 
Life Cycle > Status > Final
Life Cycle > Meta-metadata > Metadatascheme > ADL SCORM 1.2
Technical > Format > text/html
Educational > Typical Learning Time > 00:00:00
Classification > Purpose > Idea
Classification > Description > Your Title
Classification > Keyword > Your Title
```