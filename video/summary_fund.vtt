WEBVTT
Kind: captions
Language: en

00:00:01.030 --> 00:00:06.990
Welcome to the Fund Status Report lesson.
This report shows you REGIS Allocations, DELPHI

00:00:06.990 --> 00:00:13.559
Obligations, Unobligated Balance, Pending
Obligations, and Remaining Available Balance

00:00:13.559 --> 00:00:19.820
for each Region and Organization identified.
This report also allows you to filter by allocable

00:00:19.820 --> 00:00:24.960
OCCs, and is best used when an organization
allocates funds in REGIS.

00:00:26.580 --> 00:00:30.060
To access this report, click the “Reports”
button on the REGIS Launchpad.

00:00:34.059 --> 00:00:39.749
Inside the Reports module, you will see several
collapsible folders on the left. You can expand

00:00:39.749 --> 00:00:46.140
and collapse these folders individually or
you can expand them all at once by clicking

00:00:46.140 --> 00:00:48.180
the “Expand” button above the folders.

00:00:51.240 --> 00:00:53.280
Expand the “Summary Reports” folder. 

00:00:55.640 --> 00:01:02.700
Then expand the “REGIS and DELPHI Data with Allocations” sub-folder. Select the Fund Status Report.

00:01:06.420 --> 00:01:11.340
First, select your position. You can select
up to three positions concurrently for this

00:01:11.340 --> 00:01:17.260
report. You will need to tab out of the “Position”
field for the other filters to populate.

00:01:19.000 --> 00:01:24.320
The first two fields are “Start Date” and
“End Date”. These default to the current

00:01:24.320 --> 00:01:29.340
fiscal year. You can change these dates by
selecting from the drop-down lists.

00:01:30.980 --> 00:01:36.939
Next, choose your Org Code Selection. For
this example we will select, “Selected Org

00:01:36.939 --> 00:01:42.939
Code + All Detail”. For information on the
other options, see the Reports Overview.

00:01:44.700 --> 00:01:49.900
Now, determine your filters. At a minimum,
you must choose a Fund Code as a filter. 

00:01:51.020 --> 00:01:55.380
This is the only required filter but you may want to use others depending on your needs.

00:01:59.380 --> 00:02:09.580
Region, Org Code, BLI/PE, OCC, Program Name, Program Category, and AFC are other filters

00:02:09.590 --> 00:02:12.330
that you may use.  However, because

00:02:12.330 --> 00:02:18.700
this report contains DELPHI data, you do not want to select Program Name, Program Category,

00:02:18.700 --> 00:02:22.300
or AFC because it could potentially skew your results.

00:02:22.760 --> 00:02:25.480
Here we have selected the major Object Class Codes.

00:02:26.400 --> 00:02:28.700
If you have not selected a report

00:02:28.700 --> 00:02:34.320
with “Selected Org Code + All Detail”
you will not see the Region and Org Code filter options.

00:02:35.000 --> 00:02:44.020
If your organization has not fully allocated funding at the object class code level, you must include “NONE” in order to see all of your allocations.

00:02:45.180 --> 00:02:47.420
The only primary sort available in this report

00:02:47.420 --> 00:02:52.480
is “Region/Org. Code”.
If you have built scenarios, you may select

00:02:52.480 --> 00:02:58.090
one from the next drop-down list. This section
and the associated buttons are discussed in

00:02:58.090 --> 00:03:04.150
the Reports Overview.
Check the “Show Allocable OCCs” box. Doing

00:03:04.150 --> 00:03:10.739
so will include allocable OCCs on your report;
otherwise you will see data rolled up to the

00:03:10.739 --> 00:03:15.140
Region and Org Code level.
You will see a radio button that gives you

00:03:15.140 --> 00:03:20.379
the choice between “Overnight” and “Real-Time”.
We will leave this set to the default setting

00:03:20.379 --> 00:03:24.519
of “Overnight”.
Use the Wildcard Filter section to enter a

00:03:24.519 --> 00:03:29.939
DELPHI Project Number as an additional filter
if needed. This is useful if you are working

00:03:29.939 --> 00:03:35.959
with F&amp;E Direct funds.
Once you have set all your parameters, select “View”.

00:03:40.720 --> 00:03:42.700
The top section of the report shows you the

00:03:42.709 --> 00:03:47.769
parameters you selected. If you need to change
these, go back to the previous window.

00:03:49.280 --> 00:03:51.340
Now let’s discuss the columns of the report.

00:03:53.640 --> 00:03:56.900
Since we selected “Show Allocable OCCs”,

00:03:56.909 --> 00:04:03.030
this is the first column on the report.
The next column shows the Region and Org Code.

00:04:03.030 --> 00:04:07.319
The “REGIS Allocation” column shows you
the amount allocated to this Object Class Code.

00:04:08.860 --> 00:04:10.820
You can use this report to see how much money

00:04:10.829 --> 00:04:15.229
is available as long as your REGIS allocation
has been entered into the system. 

00:04:16.040 --> 00:04:20.060
Allocations must be at the Object Class level for this
report to be useful.

00:04:21.389 --> 00:04:26.759
The “REGIS Amount” column shows your REGIS
totals excluding any REGIS issued commitment

00:04:26.759 --> 00:04:33.719
(or PR) amounts. The issued amount ties back
to the “IA/PR Amount” in the Transactions module.

00:04:34.940 --> 00:04:37.720
Issued commitment (or PR) totals can be found

00:04:37.720 --> 00:04:41.400
in the next column, called “REGIS Issued
Commitment (PR)”.

00:04:42.590 --> 00:04:46.650
The “REGIS Total” column is the sum of
the previous two columns.

00:04:46.650 --> 00:04:52.949
The “DELPHI Obligations” column displays
DELPHI totals excluding PRs. As a reminder,

00:04:52.949 --> 00:05:00.699
DELPHI obligations are the sum of UDOs, AEUs,
and EXPs. A thorough discussion of these DELPHI

00:05:00.699 --> 00:05:03.659
terms can be found in the REGIS Budget Essentials
lesson.

00:05:05.040 --> 00:05:11.120
The “Unobligated Balance” column shows
the difference between REGIS Allocations and DELPHI Obligations.

00:05:12.360 --> 00:05:15.479
The next column is “Pending Obligations”.

00:05:15.479 --> 00:05:20.569
This amount is the “DELPHI Obligations”
subtracted from the “REGIS Total”.

00:05:20.569 --> 00:05:25.580
The “Remaining Available Balance” is the
difference between the previous two columns.

00:05:25.580 --> 00:05:31.270
Notice that this report shows totals at the
bottom. The first total, in blue, is the Subtotal

00:05:31.270 --> 00:05:36.690
by Region and Org Code.
The Grand Total, in purple, is your Fund total.

00:05:38.320 --> 00:05:40.480
This concludes the Fund Status Report lesson.

00:05:42.260 --> 00:05:44.800
To return to the landing page, select Back to Menu.

