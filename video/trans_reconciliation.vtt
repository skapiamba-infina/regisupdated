WEBVTT
Kind: captions
Language: en

00:00:01.209 --> 00:00:06.270
Welcome to the Reconciliation lesson. The
purpose of this lesson is to outline the process

00:00:06.270 --> 00:00:10.990
of reconciling and unreconciling transactions
between DELPHI and REGIS.

00:00:12.600 --> 00:00:17.660
Reconciliation is the process of balancing
your checkbook register, REGIS, and your bank

00:00:17.660 --> 00:00:25.340
statement, DELPHI. Reconciliation is an internal
control that allows organizations to maintain

00:00:25.349 --> 00:00:32.349
a pulse on funding, determine how much has
been funded, and how much remains to be funded.

00:00:33.200 --> 00:00:38.120
Funds Certifiers are responsible for reviewing
commitments and obligations to ensure they

00:00:38.120 --> 00:00:43.079
are in compliance with laws, policies, and
guidance, and that their organization does

00:00:43.079 --> 00:00:48.839
not obligate more than has been allotted.
If properly reconciled, a cuff record allows

00:00:48.839 --> 00:00:54.399
you to know, at any point in time, how much
funding is available. When obligations or

00:00:54.399 --> 00:01:00.659
outlays exceed the funds available, the Agency
is ‘overdrawn’. This is one way an Antideficiency

00:01:00.659 --> 00:01:05.309
Act violation occurs.
During the first three quarters of the fiscal

00:01:05.309 --> 00:01:11.979
year, reconciliation should occur at a minimum,
on a monthly basis. However, weekly reconciliation

00:01:11.979 --> 00:01:17.740
is suggested, and if you have a large workload,
will simplify the process. During the fourth

00:01:17.740 --> 00:01:23.180
quarter, reconciliation has to be stepped
up. Reconciliation is required on a weekly

00:01:23.180 --> 00:01:28.460
basis during the first two months of the fourth
quarter and on a daily basis during the last

00:01:28.460 --> 00:01:34.450
month of the fourth quarter. Funds Certifiers
should contact their Line of Business or Staff

00:01:34.450 --> 00:01:39.310
Office Senior Financial Manager to determine
if there are additional reconciliation requirements

00:01:39.310 --> 00:01:44.710
for their organization, as some may require
a more aggressive reconciliation schedule.

00:01:46.560 --> 00:01:52.460
Additional information on reconciliation can
be found in the Reconciling Transactions SOP

00:01:52.469 --> 00:01:59.969
available on the Financial Services website.
There are two reconciliation processes between

00:01:59.969 --> 00:02:06.280
the DELPHI data and the REGIS transaction
data. One is automatically done by the system;

00:02:06.280 --> 00:02:11.830
the other is manually done by the user.
Let’s talk about the automated process.

00:02:11.830 --> 00:02:17.000
Each night, the REGIS system compares REGIS
and DELPHI data and automatically reconciles

00:02:17.000 --> 00:02:24.760
transactions if certain criteria match. Fund
Code, Region, Org Code, Object Class Code,

00:02:24.760 --> 00:02:32.020
Budget Line Item, Project, and Task must match.
In addition, the REGIS Amount and DELPHI EXP

00:02:32.020 --> 00:02:33.880
amount must be identical.

00:02:37.460 --> 00:02:40.600
Periodically, REGIS users must go in and manually

00:02:40.610 --> 00:02:45.890
reconcile any transaction records that were
not automatically reconciled by the system.

00:02:46.120 --> 00:02:52.640
As mentioned before, there are requirements
on how often you must reconcile. Remember,

00:02:52.640 --> 00:02:57.090
the more frequently you reconcile, the fewer
transactions you must address at any given

00:02:57.090 --> 00:03:01.010
time.
Let’s look at a manual reconciliation for

00:03:01.010 --> 00:03:03.930
an FAA organization in Washington, DC.

00:03:06.700 --> 00:03:09.900
Click the “Reconcile” icon in the REGIS Transactions module. 

00:03:13.940 --> 00:03:18.720
A filter window opens.
Note that the “Transaction Position” and the

00:03:18.730 --> 00:03:22.930
“Filter Template” fields default to “ALL”
and “NONE”.

00:03:22.930 --> 00:03:29.840
Select a Region Code and Org Code. Further
information on how to filter this window can

00:03:29.850 --> 00:03:36.200
be found in the Advanced Filtering, Scenarios,
and Column Manipulation lesson.

00:03:36.200 --> 00:03:42.040
Then click “OK”.
This is the Reconciliation window. As with

00:03:42.040 --> 00:03:47.620
all REGIS transaction grid windows, the “Fund”
field defaults to “All Active Funds”.

00:03:47.620 --> 00:03:51.680
Because we are going to look at a transaction
that occurred during the current budget year,

00:03:51.680 --> 00:03:57.020
we will leave this field as is. We’ll discuss
the “Sort/Filter Scenario” section in

00:03:57.020 --> 00:04:01.020
the Advanced Filtering, Scenarios, and Column
Manipulation lesson.

00:04:02.140 --> 00:04:06.580
You’ll see that this window is divided into
two sections. The top section highlighted

00:04:06.590 --> 00:04:12.620
in green, displays REGIS data. The blue section
below displays DELPHI data.

00:04:12.620 --> 00:04:16.920
Please note that the Reconciliation window
defaults to unreconciled transactions. We’ll

00:04:17.440 --> 00:04:20.760
talk about the other radio button options
a little later.

00:04:21.340 --> 00:04:24.360
Now let’s look at the buttons down the right
side of the screen.

00:04:26.159 --> 00:04:31.789
Use the “Reconcile” and “Unreconcile”
buttons to reconcile and unreconcile REGIS

00:04:31.789 --> 00:04:39.159
and DELPHI transactions.
To balance DELPHI transactions, use the “Balance”

00:04:39.159 --> 00:04:44.819
button. Refer to the DELPHI Cross Reference
lesson for information on balancing.

00:04:44.819 --> 00:04:50.759
Click “Delete” to delete REGIS transactions.
The system will not allow a reconciled REGIS

00:04:50.760 --> 00:04:55.800
transaction to be deleted and will provide
a warning message should you try to do so.

00:04:55.810 --> 00:04:59.350
It will not allow you to delete a DELPHI transaction
at all.

00:05:00.140 --> 00:05:04.480
The “Filter/Sort” button reveals a window
that shows you what filters and sorts you

00:05:04.490 --> 00:05:09.110
have applied.
The next button is “Export”. Use this

00:05:09.110 --> 00:05:14.800
button to export your data to an Excel spreadsheet.
If you export from the reconciliation screen,

00:05:14.800 --> 00:05:20.270
in the exported file, you will have two tabs
on one worksheet; one for REGIS and one for

00:05:20.270 --> 00:05:26.030
DELPHI. You can find additional information on exporting in the Exporting lesson of this training.

00:05:27.940 --> 00:05:29.960
Click the “Restore/Default” button to

00:05:29.960 --> 00:05:33.820
restore the window back to the original REGIS
default configuration.

00:05:34.520 --> 00:05:39.280
Click the “Refresh” button to refresh
the data in the grid. This is helpful in those

00:05:39.289 --> 00:05:45.120
cases where more than one person has access
to the cost center code. By clicking the “Refresh”

00:05:45.120 --> 00:05:49.560
button, if new data has been entered by another
person, it will appear on the grid.

00:05:50.300 --> 00:05:54.919
The next button is “Help”. Clicking this
button opens a separate window containing

00:05:54.919 --> 00:05:57.079
helpful information on the current window.

00:06:00.840 --> 00:06:03.000
Click “Done” when you are ready to exit the window.

00:06:03.889 --> 00:06:10.529
The sum total fields will not contain data
until you select a REGIS and/or a DELPHI transaction.

00:06:10.529 --> 00:06:15.949
The variance field marked “REGIS Amount
– DELPHI EXP” must be zero in order for

00:06:15.949 --> 00:06:20.749
you to reconcile the transaction you have
selected. For example, you could not reconcile

00:06:20.749 --> 00:06:22.229
these two transactions.

00:06:25.440 --> 00:06:27.439
You will notice that most of the columns look

00:06:27.439 --> 00:06:33.060
familiar. Most of these contain the basic
REGIS and DELPHI elements. Let’s talk about

00:06:33.060 --> 00:06:36.389
those that are unique to the reconciliation
process.

00:06:36.389 --> 00:06:40.560
The “RECON” column will tell you if the
transaction has been reconciled or not if

00:06:40.560 --> 00:06:45.259
you are in the “Show All” view.
Let’s go back and click the “Show All”

00:06:45.259 --> 00:06:50.059
radio button so that you can see the various
ways REGIS identifies reconciled transactions.

00:06:52.620 --> 00:06:57.069
The “Match Type” column will tell you
how the transaction was reconciled. There

00:06:57.069 --> 00:07:02.409
are four letters that are used in this field.
“A” means that the transactions were auto-reconciled

00:07:02.409 --> 00:07:07.409
by REGIS. “R” means that the transactions
were manually reconciled, one-to-one. 

00:07:08.320 --> 00:07:14.360
“M” means the transactions were manually reconciled,
many-to-many”. Finally, “P” means that

00:07:14.370 --> 00:07:20.520
you have a possible match where certain criteria
fields were met. Let’s go back to the “Show

00:07:20.520 --> 00:07:23.560
Unreconciled” view and continue talking
about the data grid.

00:07:25.500 --> 00:07:30.860
It is divided into REGIS and DELPHI sections.
Notice there is a column for each transaction

00:07:30.870 --> 00:07:36.909
data field. Use the scroll bar to view them
all. Click on the toolbox to view the “DELPHI

00:07:36.909 --> 00:07:41.139
Column Headers” and “REGIS Column Headers”
cheat sheets with descriptions of each of

00:07:41.139 --> 00:07:47.490
the columns. This is the default view which
reflects all of the data fields. One of the

00:07:47.490 --> 00:07:53.059
benefits of REGIS is that it allows the flexibility
to customize the grid. This lets you set up

00:07:53.059 --> 00:07:58.219
the grid in a way that is useful to you. The
other nice thing about REGIS is that it will

00:07:58.219 --> 00:08:02.819
remember your preferences. When you close
the window and re-open it, it will maintain

00:08:02.819 --> 00:08:08.779
your latest view. All of the following information
can be applied to both the REGIS and DELPHI

00:08:08.779 --> 00:08:12.719
sections of the grid.
If you want to rearrange columns, you may

00:08:12.719 --> 00:08:17.689
drag and drop them. If the columns are near
each other, this may be the fastest way for

00:08:17.689 --> 00:08:22.779
you to do this. However, REGIS has a more
robust function for manipulating the data

00:08:22.779 --> 00:08:28.960
view. We’re going to show you several ways
to do this. Right-click on the column title.

00:08:28.960 --> 00:08:31.620
You will see a drop-down menu with multiple
options.

00:08:32.900 --> 00:08:37.740
“Sort Ascending” sorts your data based
on the column title that you selected. As

00:08:37.750 --> 00:08:41.669
you can see by the label, it sorts your data
by ascending order.

00:08:42.160 --> 00:08:46.160
“Sort Descending” performs the same function
in descending order.

00:08:46.160 --> 00:08:50.430
“Clear Sorting” reverts data back to the
default REGIS view.

00:08:51.520 --> 00:08:54.340
We’ll come back to the “Group by this
column” feature in a moment.

00:08:57.320 --> 00:09:03.160
“Pinned State” allows you to freeze column
panes, similar to Excel. It keeps the column

00:09:03.160 --> 00:09:09.610
that you have pinned visible while scrolling
through the rest of the grid. You’ll notice

00:09:09.610 --> 00:09:14.620
there are three options. They are “Unpin
column”, “Pin at Left”, and “Pin at

00:09:14.620 --> 00:09:20.860
Right”. The menu defaults to “Unpin column”.
Just like the “Pin at Left” option, you

00:09:20.860 --> 00:09:23.720
can pin a column to the right-hand side of
your screen. 

00:09:26.600 --> 00:09:28.280
REGIS will allow you to pin more

00:09:28.290 --> 00:09:33.620
than one column to the right or left. “Unpin”
will allow the screen to go back to the default

00:09:33.620 --> 00:09:34.180
view.

00:09:37.540 --> 00:09:42.220
“Best Fit” adjusts the width of a column
to best fit the data in that column.

00:09:42.650 --> 00:09:45.560
“Filter” takes you back to your current
filter window.

00:09:46.220 --> 00:09:51.620
“New Filter” pulls up a new blank filter.
It deletes any filters you have already applied.

00:09:51.620 --> 00:09:55.670
“Sort” allows you to sort the data by
up to eight different fields.

00:09:55.670 --> 00:10:00.280
“Show/Hide Columns” allows you to customize
which columns appear in your grid.

00:10:00.820 --> 00:10:05.680
“Autofit Columns” is similar to “Best
Fit” but it adjusts all column widths as

00:10:05.690 --> 00:10:07.790
opposed to just the selected column.

00:10:09.040 --> 00:10:13.940
“Clear All Column Filters” clears all
column filters you have applied. We will discuss

00:10:13.950 --> 00:10:16.320
column filters later in this lesson.

00:10:17.480 --> 00:10:23.620
“Re-order DELPHI/REGIS Window” puts your
DELPHI and REGIS windows side-by-side as opposed

00:10:23.620 --> 00:10:28.220
to one above the other. Click this option
again to restore the original view.

00:10:30.160 --> 00:10:34.570
The last seven options we just discussed are
also available by right-clicking anywhere

00:10:34.570 --> 00:10:38.090
in the transaction grid as opposed to in the
column title fields.

00:10:39.900 --> 00:10:45.080
This is the header bar. In addition to rearranging
the order of the columns, you can drag column

00:10:45.080 --> 00:10:47.360
titles to the header bar to create groupings.

00:10:49.920 --> 00:10:51.920
You can also create groupings by right-clicking

00:10:51.930 --> 00:10:54.710
the column header and selecting “Group by
this column”.

00:10:56.020 --> 00:11:00.700
You can arrange column titles in two ways.
You can create a parent - child relationship

00:11:00.700 --> 00:11:03.120
by placing one column title below the other.

00:11:07.040 --> 00:11:09.700
You can also create a peer-to- peer relationship

00:11:09.710 --> 00:11:15.190
by placing one column title on top of the
other. Remember, when you log out of REGIS,

00:11:15.190 --> 00:11:19.850
the system will save your configuration up
to this point. You will see this view the

00:11:19.850 --> 00:11:20.930
next time you log in.

00:11:21.800 --> 00:11:23.820
You can expand the groupings by clicking here.

00:11:25.060 --> 00:11:29.540
Next to each column title there is a symbol
that looks like a funnel. Clicking this icon

00:11:29.540 --> 00:11:34.690
will open a drop-down menu with additional
filtering options. These options vary by column

00:11:34.690 --> 00:11:40.210
or data type. Applying these column filters
does not override the window filters that

00:11:40.210 --> 00:11:46.070
you previously selected. It applies new filters
on top of the existing filters. Once you have

00:11:46.070 --> 00:11:49.770
applied a filter in a column, the column filter
icon will appear orange.

00:11:51.370 --> 00:11:54.910
Click "Restore Default” to remove any filters
you may have added to the window.

00:11:58.090 --> 00:12:02.760
You can also select multiple transactions
in one of two ways. If the transactions you

00:12:02.760 --> 00:12:06.550
want to select are consecutive, you can hold
down the “Shift” key and the down arrow

00:12:06.550 --> 00:12:12.080
to select the group. If the transactions are
non-consecutive, you can hold down your “Control”

00:12:12.080 --> 00:12:17.130
key and click on the transactions you wish
to select. Selecting multiple transactions

00:12:17.130 --> 00:12:21.050
will populate the fields at the bottom of
the screen with running subtotals.

00:12:22.260 --> 00:12:27.390
Now let’s reconcile a transaction.
Select the “Show Unreconciled” radio button

00:12:27.390 --> 00:12:34.950
to see all unreconciled transactions. REGIS
allows you to reconcile one-to-one, one-to-many,

00:12:34.950 --> 00:12:40.780
many-to-one, and many-to-many. You can reconcile
as many transactions as you want, as long

00:12:40.780 --> 00:12:46.420
as all REGIS reconciliation criteria match.
It is easy to reconcile a large group of data

00:12:46.420 --> 00:12:51.600
together. However, if you need to go back
and unreconcile any transaction in the group,

00:12:51.600 --> 00:12:57.420
the entire group will be unreconciled. In
addition, unreconciling large groups of transactions

00:12:57.420 --> 00:13:02.690
slows down the REGIS process and can take
considerable time. It is advised that you

00:13:02.690 --> 00:13:07.010
reconcile transactions in the smallest grouping possible.

00:13:07.010 --> 00:13:11.920
For this example, let’s reconcile one REGIS
transaction to one DELPHI transaction where

00:13:11.920 --> 00:13:16.860
one or more of the key fields do not match.
We have selected two records.

00:13:23.340 --> 00:13:24.560
Click the “Reconcile” button.

00:13:28.500 --> 00:13:31.160
We have received an error message. Error messages

00:13:31.170 --> 00:13:36.290
tell you what key elements do not match. In
this example, the Task Number does not match.

00:13:37.550 --> 00:13:42.090
Now comes the fun part of reconciliation.
Review the two records you have selected and

00:13:42.090 --> 00:13:46.550
verify that they are the ones you want to
reconcile. We have looked at the records and

00:13:46.550 --> 00:13:51.980
believe these are the ones we want to reconcile.
Now, we need to go back to the source document

00:13:51.980 --> 00:13:56.880
and look at the line of accounting that was
funds certified. You will notice that we entered

00:13:56.880 --> 00:14:02.200
the incorrect Task Number in the REGIS record.
Some of the REGIS fields are editable from

00:14:02.200 --> 00:14:07.970
this grid. These fields are the “Amount”,
“PR Number”, “Vendor”, “Comments”,

00:14:07.970 --> 00:14:14.170
“Travel Type”, “PCPS Number”, “Travel
Destination”, “Fund Cert Comments”,

00:14:14.170 --> 00:14:15.790
“Reference” and “PO Number”.

00:14:16.780 --> 00:14:21.760
In this case, we need to fix the Task Number,
which cannot be edited from the grid. To fix

00:14:21.760 --> 00:14:25.480
the record, double-click on the transaction
and modify the Task Number.

00:14:30.320 --> 00:14:33.180
Click “Save and Exit” to return to the
Reconciliation window.

00:14:35.570 --> 00:14:40.680
Now that you have a correct REGIS transaction
record, select your REGIS and DELPHI transactions

00:14:40.680 --> 00:14:42.060
and click “Reconcile”.

00:14:44.160 --> 00:14:49.540
Our reconciliation was successful so the transactions
were removed from the Unreconciled view.

00:14:50.470 --> 00:14:55.250
One of the nice features of REGIS is that
you can append a comment to your DELPHI transaction.

00:14:56.230 --> 00:15:00.340
In the DELPHI section of the grid, double-click
on a transaction and a “Comments” section

00:15:00.340 --> 00:15:06.790
will display. You can enter comments for this
particular transaction and save them to the

00:15:06.790 --> 00:15:12.470
record in REGIS. When you enter comments,
the row will turn orange. The orange color

00:15:12.470 --> 00:15:14.630
will disappear if you delete the comments.


00:15:20.840 --> 00:15:23.140
At no point will this comment go back to DELPHI

00:15:24.120 --> 00:15:29.680
It’s important to note that DELPHI data
is imported into REGIS. REGIS does not export

00:15:29.680 --> 00:15:35.440
to DELPHI. Any comments you attach to your
DELPHI data will only appear in REGIS. 

00:15:39.280 --> 00:15:44.680
Next we will outline the process of unreconciling
transactions between DELPHI and REGIS.

00:15:45.300 --> 00:15:49.340
If you need to unreconcile a record for any
reason, click the “Show All” button.

00:15:52.660 --> 00:15:58.520
Select a REGIS transaction to unreconcile.
You may not select both a REGIS and DELPHI

00:15:58.530 --> 00:16:02.500
transaction at the same time.
If you want to see the reconciled records

00:16:02.500 --> 00:16:08.990
before you unreconcile them, highlight either
the REGIS transaction or DELPHI transaction.

00:16:08.990 --> 00:16:12.790
Next, click the “Show Possibles and Reconciled”
radio button. 

00:16:15.600 --> 00:16:17.100
REGIS automatically locates

00:16:17.110 --> 00:16:23.280
the REGIS and DELPHI transactions that have
been reconciled as a group. Now, select the

00:16:23.280 --> 00:16:26.180
transaction that you wish to unreconcile,
and click “Unreconcile”. 

00:16:30.020 --> 00:16:31.240
This will unreconcile

00:16:31.250 --> 00:16:35.600
all corresponding records.
Once we have completed this action, you can

00:16:35.600 --> 00:16:39.650
return to either “Unreconciled” or “Show
All”.

00:16:39.650 --> 00:16:43.810
We will select “Show All” so that we can
show you what the transaction now looks like.

00:16:45.990 --> 00:16:51.130
Note that the “RECON” column now displays
“NO” for the records we just unreconciled.

00:16:52.010 --> 00:16:56.250
You do not have to use the “Show Possibles
and Reconciled” feature. You can select

00:16:56.250 --> 00:17:01.130
the record you want to unreconcile directly
from this grid and click “Unreconcile”.

00:17:01.130 --> 00:17:06.829
In this case, REGIS will unreconcile the corresponding
records without displaying them.

00:17:06.829 --> 00:17:09.769
When you are finished, click “Done” to
exit this window.

00:17:10.939 --> 00:17:15.669
This concludes the Reconciliation lesson.
For details on reconciliation by transaction

00:17:15.669 --> 00:17:22.350
type, please refer to the Advanced Reconciliation
lesson. To return to the landing page, select

00:17:22.350 --> 00:17:23.039
Back to Menu.

