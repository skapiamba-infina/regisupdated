WEBVTT
Kind: captions
Language: en

00:00:00.380 --> 00:00:05.950
Welcome to the Customized Report lesson. Unlike
the other reports you may have seen, this

00:00:05.950 --> 00:00:11.150
is a more interactive and flexible tool to
help you meet your reporting needs. Think

00:00:11.150 --> 00:00:17.159
of this as your one stop shop for reporting
needs. This tool has much of the same functionality

00:00:17.159 --> 00:00:19.899
as the pivot functionality in Excel.

00:00:20.540 --> 00:00:23.200
This report can be found in the Reports module. 

00:00:27.560 --> 00:00:30.279
To access this module, click on the “Reports”

00:00:30.279 --> 00:00:33.280
button on the REGIS Launchpad.

00:00:33.280 --> 00:00:39.090
Inside the Reports module, you will see several
collapsed folders on the left side. You can

00:00:39.090 --> 00:00:42.310
expand and collapse these folders individually 

00:00:45.820 --> 00:00:48.280
or you can expand them all at once by clicking

00:00:48.290 --> 00:00:51.260
the “Expand” button above the folders.

00:00:51.840 --> 00:00:54.160
Expand the “Pivot Report” folder. 

00:00:55.800 --> 00:00:57.800
Then select the Customized Report.

00:00:59.250 --> 00:01:04.390
The first field is the “Report” drop-down
list. Here you will see a list of all summary

00:01:04.390 --> 00:01:09.999
level reports that you can run from one report
filter window. In this example we will select

00:01:09.999 --> 00:01:16.979
the Scorecard Report. If you have built scenarios,
you may select one from the drop-down list.

00:01:16.979 --> 00:01:22.920
“Scenarios” and the associated buttons
are discussed in the Reports Overview.

00:01:22.920 --> 00:01:28.249
Select your position. You can select up to
three positions. You need to tab out of the

00:01:28.249 --> 00:01:32.219
“Position” field for the other filters to populate.

00:01:32.219 --> 00:01:37.329
You will see a radio button that gives you
the choice between “Overnight” and “Real-Time”.

00:01:37.329 --> 00:01:41.369
We will leave this set to the default setting
of “Overnight”.

00:01:42.360 --> 00:01:48.360
Next, choose your Org Code Selection. For
this example, we will use the “Selected

00:01:48.439 --> 00:01:55.119
Org Code Only + All Detail” option. For
information on other options, see the Reports

00:01:55.119 --> 00:01:55.770
Overview.

00:01:56.820 --> 00:02:02.060
The “Start Date” and “End Date” fields
default to the current fiscal year. However,

00:02:02.079 --> 00:02:05.759
you can change them by selecting from the
drop-down lists.

00:02:06.450 --> 00:02:11.870
The Wildcard Filter section allows you to
enter a DELPHI Project Number as an additional

00:02:11.870 --> 00:02:16.530
filter. This would be useful if you are working
with F&amp;E Direct funds.

00:02:16.530 --> 00:02:22.810
Now, determine your filters. In this report,
you must expand and collapse each filter field

00:02:22.810 --> 00:02:29.810
by clicking the double arrows on the side.
At a minimum, you must choose a Fund Code

00:02:30.220 --> 00:02:35.630
as a filter. This is the only required filter
but you may want to use others depending on

00:02:35.630 --> 00:02:36.990
your needs.

00:02:38.220 --> 00:02:46.720
Region, Org Code, BLI/PE, OCC, Program Name,
Program Category, and AFC are other filters

00:02:46.730 --> 00:02:53.180
that you may use. If you have not selected
a report with “Selected Org Code + All Detail”

00:02:53.180 --> 00:02:57.200
you will not see the “Region” and “Org
Code” filter options populate.

00:02:58.140 --> 00:03:02.380
Once you have set all of your parameters,
click the “View Data” button at the bottom

00:03:02.380 --> 00:03:04.890
to view your report.

00:03:04.890 --> 00:03:09.230
When the report runs, it replaces your original
filter window on the screen. 

00:03:13.660 --> 00:03:19.120
You will still see the “Filter” button which we will
discuss later. You will also still see your

00:03:19.120 --> 00:03:23.450
“Report” and “Scenario” drop-down
lists at the top of the screen. Selecting

00:03:23.450 --> 00:03:28.350
from these lists allows you to move seamlessly
between different reports or choose different

00:03:28.350 --> 00:03:33.930
scenarios without resetting your filters.
We will discuss this in a bit.

00:03:33.930 --> 00:03:40.350
Now let’s discuss the report. First we will
talk about the basic structure of the report.

00:03:40.350 --> 00:03:44.760
We have shown you how to set your basic filters
and later we will show you some additional

00:03:44.760 --> 00:03:49.850
ways to customize this report. Let’s look
at the columns of the report.

00:03:49.850 --> 00:03:54.430
The first couple of columns, in blue, group
the data. They will populate based on the

00:03:54.430 --> 00:04:00.940
type of report you selected. This is a Scorecard
Report so the data is grouped by Fund Code,

00:04:00.940 --> 00:04:05.050
Org Code, and two-character OCC.

00:04:05.050 --> 00:04:09.440
After the filter columns, there are several
columns specific to the report template you

00:04:09.440 --> 00:04:17.980
chose. In our case, this was the Scorecard
Report template. The “REGIS%DELPHI” column

00:04:17.980 --> 00:04:22.040
shows you the REGIS total divided by the DELPHI
total.

00:04:23.520 --> 00:04:30.500
You’ll notice this column is color-coded.
Green indicates 95% or more. Yellow indicates

00:04:30.500 --> 00:04:39.100
85 - 94%. If your rate is less than 85%, it
will be highlighted in Red. If you are cuffing

00:04:39.100 --> 00:04:44.620
as you funds certify and reconciling regularly,
your report will be in the green. If you have

00:04:44.630 --> 00:04:50.620
an area that is highlighted in yellow, it
probably requires some attention. If you have

00:04:50.620 --> 00:04:56.430
an area in red, this indicates there are problems
with your data. One exception to this “red

00:04:56.430 --> 00:05:01.310
is bad” guideline is that if you have no
transactions in REGIS and no transactions

00:05:01.310 --> 00:05:07.940
in DELPHI, both totals will be zero and the
column will be red. As of early FY14, this

00:05:07.940 --> 00:05:12.020
is a bug in REGIS and it will be fixed in
a subsequent release.

00:05:12.720 --> 00:05:18.100
The “REGIS Amount” column shows your REGIS
totals excluding any REGIS issued, commitment

00:05:18.100 --> 00:05:24.000
(or PR) amounts. The issued amount ties back
to the “IA/PR” amount in the Transactions

00:05:24.000 --> 00:05:25.310
module.

00:05:25.940 --> 00:05:32.300
Issued, commitment (or PR) totals can be found
in the next column, called “REGIS Commit”.

00:05:32.980 --> 00:05:36.720
The “REGIS Total” column is the sum of
the previous two columns.

00:05:37.200 --> 00:05:39.180
Now let’s look at the DELPHI columns.

00:05:40.790 --> 00:05:47.180
The “DELPHI Obligations” column gives
you your DELPHI totals excluding PRs. As a

00:05:47.180 --> 00:05:54.150
reminder, DELPHI obligations are the sum of
UDOs, AEUs, and EXPs. A thorough discussion

00:05:54.150 --> 00:06:00.300
of these DELPHI terms can be found in the
REGIS Budget Essentials lesson.

00:06:00.300 --> 00:06:05.050
The total for PRs still in the commitment
phase can be found in the “DELPHI Commitments”

00:06:05.050 --> 00:06:05.699
column.

00:06:06.140 --> 00:06:09.900
The “DELPHI Total” column is the sum of
these two columns.

00:06:10.850 --> 00:06:15.530
The final column to the right is the REGIS
total minus the DELPHI total.

00:06:17.240 --> 00:06:22.480
Now let’s go back to what you can do with
this report. There are many ways to modify

00:06:22.480 --> 00:06:28.590
and view the data that you see in this report.
You can apply or remove filters. You can re-order

00:06:28.590 --> 00:06:34.330
the look and feel of the report. And you can
use this report to drill down to source-level

00:06:34.330 --> 00:06:36.990
data. We will look at each of these separately.

00:06:38.280 --> 00:06:43.600
Let’s do some analysis on this data to illustrate
some of the functionality within Customized

00:06:43.610 --> 00:06:53.570
Reports. Notice that Org Code WAC1100000 has
a lot of red. This indicates a possible problem.

00:06:54.320 --> 00:07:00.400
Let’s filter this data for WAC1100000.

00:07:00.400 --> 00:07:05.960
We can filter one of two ways. To re-open
the entire filter screen and make adjustments

00:07:05.960 --> 00:07:11.620
to your original parameters, hit the “Filter”
button on the top right of the screen. Make

00:07:11.620 --> 00:07:15.139
your changes and click “View Data” again.

00:07:15.139 --> 00:07:20.229
Another way to filter is to use the filter
funnels. Clicking one of these funnels allows

00:07:20.229 --> 00:07:25.199
you to select or de-select parameters that
are currently feeding this report.

00:07:25.660 --> 00:07:31.620
Let’s use the filter funnels to narrow down
our selection on “Region” and “Org Code”.

00:07:31.620 --> 00:07:42.620
Click the filter funnel. De-select all. Scroll
down and click on WAC1100000. Alternately,

00:07:42.620 --> 00:07:50.000
you could type WAC1100000 at the top of the
filter and the system will filter for what

00:07:50.009 --> 00:07:58.289
you are typing as you type it. Click “OK”.
Notice that in this report, PC&amp;B is included

00:07:58.290 --> 00:08:04.889
in the object class code data displayed. The
vast majority of PC&amp;B is automatically imported

00:08:04.889 --> 00:08:10.979
and reconciled in REGIS. This might throw
off our total percentages. If we wanted to

00:08:10.979 --> 00:08:16.539
exclude PC&amp;B, we could click on the “OCC
2 Character” filter funnel.

00:08:20.360 --> 00:08:26.560
We will de-select Object Class Codes 11 and
12 and click “OK” to remove the PC&amp;B.

00:08:27.160 --> 00:08:31.900
You will notice that when you have filtered
a column, the filter funnel turns orange.

00:08:31.900 --> 00:08:37.690
So in other words, gray funnels have not been
used and orange ones have. Both “Org Code”

00:08:37.690 --> 00:08:43.560
and “OCC 2 Character” are now orange.
It looks like our problem is in Object Class

00:08:43.560 --> 00:08:44.460
Code 21.

00:08:46.029 --> 00:08:49.430
Let’s filter for just that object class
code.

00:08:59.260 --> 00:09:00.440
You can use the filters

00:09:00.450 --> 00:09:05.440
in the upper section of the report to modify
the source data or change the look and feel

00:09:05.440 --> 00:09:11.390
of the report. We are going to left-click
the “OCC” box here and drag it to the

00:09:11.390 --> 00:09:17.310
right of the “OCC 2 Character” column.
We see a little more data for Object Class

00:09:17.310 --> 00:09:22.800
Code 21 but we still can’t see where the
problem is. The nice thing about this report

00:09:22.800 --> 00:09:27.130
is that you can double-click on the “Amount”
field and the report will bring up the data

00:09:27.130 --> 00:09:30.950
at a transaction level. Let's scroll

00:09:30.950 --> 00:09:36.210
and look at the names of the travelers. It
looks like many people have been traveling.

00:09:38.440 --> 00:09:41.160
Let's filter for Cindee Rykhus. 

00:09:48.720 --> 00:09:53.220
REGIS indicates
$8K has been cuffed for Cindee's travel.

00:09:53.220 --> 00:09:55.260
Let’s close this screen. 

00:09:59.360 --> 00:10:02.760
Just like with REGIS data, we can double-click on DELPHI

00:10:02.760 --> 00:10:09.410
data and pull up the associated obligations.
Let's filter for Cindee again, this time looking

00:10:09.410 --> 00:10:11.130
at DELPHI data. 

00:10:28.860 --> 00:10:34.920
DELPHI indicates there is
$12K in travel for Cindee. The scorecard indicated

00:10:34.930 --> 00:10:39.990
that there were more records in DELPHI than
had been entered into REGIS. The detailed

00:10:39.990 --> 00:10:45.000
data helps us identify what data didn’t
get entered. The Funds Certifier responsible

00:10:45.000 --> 00:10:50.700
for this fund needs to analyze and take corrective
actions on the data where needed. In this

00:10:50.700 --> 00:10:56.050
case, it looks like the Funds Certifier or
designee needs to go into REGIS and add the

00:10:56.050 --> 00:11:01.649
travel events for all of the missing travelers.
While we are in this window, let’s explore

00:11:01.649 --> 00:11:04.109
some additional functionality. 

00:11:06.700 --> 00:11:10.200
This is the
default view which reflects all of the data

00:11:10.209 --> 00:11:15.510
fields. One of the benefits of REGIS is that
it allows the flexibility to customize the

00:11:15.510 --> 00:11:20.430
grid. Just like we discussed in the Transaction
Register lesson, this lets you set up the

00:11:20.430 --> 00:11:25.600
grid in a way that is useful to you. If you
want to rearrange columns, you may drag and

00:11:25.600 --> 00:11:30.560
drop them. If the columns are near each other,
this may be the fastest way for you to do

00:11:30.560 --> 00:11:36.860
this. However, REGIS has a more robust function
for manipulating the data view. We’re going

00:11:36.860 --> 00:11:39.360
to show you several ways to do this. 

00:11:44.440 --> 00:11:48.280
Right-click
on the “Vendor” column title. The names

00:11:48.290 --> 00:11:52.930
of the travelers appear in this column in
REGIS. You will see a drop-down menu with

00:11:52.930 --> 00:11:58.890
various options. “Sort Ascending” sorts
your data based on the column title that you

00:11:58.890 --> 00:12:04.089
selected. As you can see by the label, it
sorts your data by ascending order. “Sort

00:12:04.089 --> 00:12:08.990
Descending” performs the same function in
descending order. “Clear Sorting” reverts

00:12:08.990 --> 00:12:15.310
data back to the default REGIS sort. We’ll
come back to the “Group by this column”

00:12:15.310 --> 00:12:20.649
feature in a moment. “Column Chooser”
allows you to remove column headers or add

00:12:20.649 --> 00:12:25.839
ones that have been hidden from the current
view. This is very similar to the “Show/Hide”

00:12:25.839 --> 00:12:31.459
function in the Transactions module. You can also get rid of columns by selecting “Hide Column”.

00:12:35.000 --> 00:12:39.720
 To bring back a hidden column,
you must use the “Column Chooser” function.

00:12:54.260 --> 00:12:59.880
“Pinned State” allows you to freeze column
panes, similar to Excel. It keeps the column

00:12:59.890 --> 00:13:04.950
that you have pinned visible while scrolling
through the rest of the grid. You’ll notice

00:13:04.950 --> 00:13:10.090
there are three options. They are “Unpin
Column”, “Pin at Left”, and “Pin at

00:13:10.090 --> 00:13:16.700
Right”. The menu defaults to “Unpin Column”.
Let’s click on “Pin at Left” in the

00:13:16.700 --> 00:13:18.860
“Org Code” column. 

00:13:21.480 --> 00:13:24.760
Now you’ll note that
when we scroll to the right, the left-hand

00:13:24.760 --> 00:13:26.600
column stays visible. 

00:13:30.720 --> 00:13:34.700
Just like the "Pin at
Left" column you can pin a column to the right-hand

00:13:34.709 --> 00:13:39.909
side of your screen. REGIS will allow you
to pin more than one column to the right or left. 

00:13:46.930 --> 00:13:50.210
“Unpin” will allow the screen to
go back to the default view. 

00:13:59.840 --> 00:14:07.420
“Best Fit” adjusts the width of a column to best fit
the data in that column. In addition to rearranging

00:14:07.430 --> 00:14:12.850
the order of the columns, you can drag column
titles to the header bar to create groupings.

00:14:20.980 --> 00:14:26.060
You can also create groupings by right-clicking
the column header and selecting “Group by

00:14:26.060 --> 00:14:32.480
this column”. You can arrange column titles
in two ways. You can create a parent - child

00:14:32.480 --> 00:14:39.220
relationship by placing one column title below the other. Let’s group our data by invoice number.

00:14:43.960 --> 00:14:46.860
 Remember the traveler’s name is
in the “Vendor” column. 

00:14:54.440 --> 00:14:55.760
You can also create

00:14:55.769 --> 00:15:01.060
a peer-to- peer relationship by placing one
column title on top of the other. Let’s

00:15:01.060 --> 00:15:06.470
add the PO number, which is the field where
the TA number can be found. You can export

00:15:06.470 --> 00:15:12.209
this detail window to an Excel workbook. If
you export from this window, you will not

00:15:12.209 --> 00:15:17.800
get an Excel workbook that has the exact same
formatting as appears on the screen. When

00:15:17.800 --> 00:15:22.700
you export the data, you will get the raw
data for the transactions you have selected.

00:15:22.700 --> 00:15:28.790
If you had hidden columns, they will appear
in the export. If you had grouped data, the

00:15:28.790 --> 00:15:34.079
groupings will not appear. However, if you
had rearranged columns, the export will keep

00:15:34.079 --> 00:15:39.930
the columns in the order you had selected.
More details on this can be found in the Exporting

00:15:39.930 --> 00:15:46.240
lesson. Click on “Help” for additional
information on this window. To get out of

00:15:46.240 --> 00:15:52.579
this window and return to the Customized Report,
click “Close”. Now we are back to the

00:15:52.579 --> 00:15:58.110
data grid. Right-clicking any of these column
headers will open another menu. As we said

00:15:58.110 --> 00:16:03.620
before, “Best Fit” adjusts the width of
a column to best fit the data in that column.

00:16:03.620 --> 00:16:08.220
“Expand All” and “Collapse All” will
expand or collapse the groupings that you

00:16:08.220 --> 00:16:09.900
have created in your report. 

00:16:15.500 --> 00:16:17.500
“Reload Data” refreshes the data.

00:16:22.060 --> 00:16:23.340
 “Show Field List”

00:16:23.350 --> 00:16:27.880
pulls up a list of additional fields from
the Transaction Register that were not used

00:16:27.880 --> 00:16:33.180
in the default report. You can add any of
these to the report in various places, seen

00:16:33.180 --> 00:16:35.400
on the bottom menu. 

00:16:37.760 --> 00:16:40.880
Finally, the bottom of
the main screen will have many of the same

00:16:40.880 --> 00:16:46.820
buttons as the other reports. One thing to
note is that if you export from this screen,

00:16:46.820 --> 00:16:51.950
you will get a workbook that has the same
format as the data grid you have customized.

00:16:51.950 --> 00:16:57.339
This is different than the export function
discussed earlier in the Detail window. One

00:16:57.339 --> 00:17:01.820
really nice feature in the print capability
for this report is that, unlike with other

00:17:01.820 --> 00:17:07.160
REGIS reports, you have several additional
print options. “Print” allows you to print

00:17:07.160 --> 00:17:11.980
to your local printer. “Print Preview”
will display a preview of what your report

00:17:11.980 --> 00:17:18.250
will look like when it’s printed. Note the
blue banner at the top of your “Print Preview”

00:17:18.250 --> 00:17:24.900
screen. “Print” sends the report to the
default printer. “Print Settings” opens

00:17:24.900 --> 00:17:31.220
a window for the default printer and allows
changes to the print set-up. “Watermark”

00:17:31.220 --> 00:17:36.780
opens a window to allow you to place a watermark,
such as DRAFT, on the report. “Previous

00:17:36.780 --> 00:17:42.020
Page” takes you to the previous page of
a multiple-page report. “Next Page” takes

00:17:42.020 --> 00:17:48.299
you to the next page of a multiple-page report.
“Go To Page” allows you to enter the page

00:17:48.299 --> 00:17:54.270
number you wish to view in a multiple-page
report. “Of Pages” is the total number

00:17:54.270 --> 00:18:01.070
of pages in the report. “Zoom Out” adjusts
the view further away. Using this will change

00:18:01.070 --> 00:18:07.910
the percentage in the “Percentage” zoomed
box. “Zoom In” adjusts the view closer.

00:18:07.910 --> 00:18:13.990
Using this will change the percentage in the
“Percentage” zoomed box. “Percent Zoomed”

00:18:13.990 --> 00:18:18.650
displays the percentage the view has increased
or decreased if the “Zoom Out” or “Zoom

00:18:18.650 --> 00:18:24.470
In” feature was used. You can also select
this from the drop-down list. “Layout”

00:18:24.470 --> 00:18:32.020
is how the page is viewed on the screen. The
default is 1 x 1, or one-page view. You might

00:18:32.020 --> 00:18:37.730
change this to see more pages at a time in
a multiple-page report. You can also select

00:18:37.730 --> 00:18:39.510
this from the drop-down list. 

00:18:44.400 --> 00:18:47.480
“Print Settings”
allows you to change the format, including

00:18:47.490 --> 00:18:53.080
print order, settings, fonts, and colors.
“Print Setting” also allows you to adjust

00:18:53.080 --> 00:18:59.320
your paper including page type, margins and
orientation. The recommendation when printing

00:18:59.320 --> 00:19:05.490
or previewing these reports is to select the
“Landscape” orientation. And finally,

00:19:05.490 --> 00:19:10.500
“Print Settings” allows you to customize
your headers and footers. By default, the

00:19:10.500 --> 00:19:14.460
report name will be centered in the header,
your initial filters will be listed in the

00:19:14.460 --> 00:19:19.330
left footer, and the date and time that you
ran the report will be in the right footer.

00:19:19.330 --> 00:19:24.549
As we mentioned at the beginning, you can
run this customization for many other reports

00:19:24.549 --> 00:19:30.080
as seen in the original drop-down list but
there is one final shortcut we can show you.

00:19:30.080 --> 00:19:34.580
If you were to go to the top of your screen
and select another report from the drop-down

00:19:34.580 --> 00:19:39.780
list, the system will run the new report with
all of the filter parameters you had previously

00:19:39.780 --> 00:19:45.110
selected in the initial report. It will not
keep secondary filters that you applied via

00:19:45.110 --> 00:19:52.110
the filter funnels. Let’s demonstrate this
by selecting the OCC Obligation Status Report

00:19:52.299 --> 00:19:57.400
and clicking “View Data”. You can see
that the initial filters we had selected for

00:19:57.400 --> 00:20:03.919
the Scorecard Report are reflected here in
the new report. It’s obvious that this is

00:20:03.919 --> 00:20:10.370
a very flexible and powerful tool for analyzing
both REGIS and DELPHI data. It’s ad hoc

00:20:10.370 --> 00:20:17.150
reporting at its best. This concludes the
Customized Report lesson. To return to the

00:20:17.150 --> 00:20:20.270
landing page, select Back to Menu.

