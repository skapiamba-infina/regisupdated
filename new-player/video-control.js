$(window).load(function () {
    $(".html5-player").each(function () {
        var id = $(this).attr("id"), wrapperID = $(this).attr("wrapper_id"), video = $(this).find("source").attr("src"),
            title = $(this).attr("video_title")


        var player = $(this).mediaelementplayer({
            // shows debug errors on screen
            enablePluginDebug: true,
            // Hide/Show controls when playing and mouse is not over the video
            alwaysShowControls: false,
            canSeekForward: true,
            // rewind to beginning when media ends
            autoRewind: false,
            enableAutosize: false,
            // default if the <video width> is not specified
            defaultVideoWidth: 720,
            // default if the <video height> is not specified     
            defaultVideoHeight: 450,
            mode: 'auto_plugin',

            // method that fires when the Flash or Silverlight object is ready
            success: function (mediaElement, domObject) {
                mediaElement.addEventListener('ended', function (e) {

                }, false);

                mediaElement.addEventListener('timeupdate', function () {

                }, false);

                $(".show-window", parent.document).click(function () {
                    mediaElement.pause();
                });

                //mediaElement.captionsButton.off("mouseenter");


                //Setup the video and subtitle src
                //mediaElement.options.toggleCaptionsButtonWhenOnlyOne = true;
                /*mediaElement.findTracks();
                mediaElement.loadTrack(0);
                mediaElement.setSrc(videoSource);
                mediaElement.addEventListener('loadeddata', function (e) {
                    setTimeout(function () { $(".mejs-overlay-button").trigger("click"); }, 100);
                });*/
                playerElement = mediaElement;
            }
        });
    });
});